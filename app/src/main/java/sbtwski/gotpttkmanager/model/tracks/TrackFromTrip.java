package sbtwski.gotpttkmanager.model.tracks;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.util.Date;

import sbtwski.gotpttkmanager.model.RecyclerCompatible;

/**
 * Klasa używana do przechowywania informacji dotyczących trasy będącej częścią wycieczki.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class TrackFromTrip implements Serializable, RecyclerCompatible {
    private Date startDate;
    private Date endDate;
    private boolean isCustom, isUp;
    private int id;
    private Track track;

    public TrackFromTrip() {
        startDate = new Date();
        endDate = new Date();
        track = new TrackPTTK();
    }

    public TrackFromTrip(int id, Date startDate, Date endDate, boolean isCustom, boolean isUp, Track track) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.isCustom = isCustom;
        this.isUp = isUp;
        this.id = id;
        this.track = track;
    }

   /**
    * Metoda zwracająca tytuł elementu znajdującego się na liście.
    */
   @Override
   public String getItemTitle() {
        return track.getItemTitle();
    }

   /**
    * Metoda zwracająca pierwszy podtytuł elementu znajdującego się na liście.
    */
   @Override
   public String getFirstSubsection() {
        return track.getFirstSubsection();
    }

   /**
    * Metoda zwracająca drugi podtytuł elementu znajdującego się na liście.
    */
   @Override
   public String getSecondSubsection() {
        return track.getSecondSubsection();
    }

   public Date getStartDate() {
        return startDate;
    }

   public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

   public Date getEndDate() {
        return endDate;
    }

   public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

   public boolean isCustom() {
        return isCustom;
    }

   public void setCustom(boolean custom) {
        isCustom = custom;
    }

   public boolean isUp() {
        return isUp;
    }

   public void setUp(boolean up) {
        isUp = up;
    }

   public int getId() {
        return id;
    }

   public void setId(int id) {
        this.id = id;
    }

   public Track getTrack() {
       return track;
   }

   public void setTrack(Track track) {
       this.track = track;
   }

   /**
    * Metoda zwracająca odpowiednią wartość punktową trasy w zależności od tego czy trasa znajduje się w wykazie czy jest trasą własną.
    */
   private int getPoints() {
      int points;
      if(isCustom)
          points = ((TrackCustom)track).getPoints();
      else
          points = isUp ? ((TrackPTTK)track).getPointsUp() : ((TrackPTTK)track).getPointsDown();

      return points;
   }

   public String getPointsText(){
       return getPoints()+"";
   }
}
