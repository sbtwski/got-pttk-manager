package sbtwski.gotpttkmanager.model.tracks;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import sbtwski.gotpttkmanager.model.RecyclerCompatible;

/**
 * Klasa używana do przechowywania informacji dotyczących trasy znajdującej się w wykazie tras punktowanych PTTK.
 */

public class TrackPTTK extends Track implements Serializable, RecyclerCompatible {
    private int pointsUp, pointsDown;
    private Date validFrom;
    private Date validTo;

    public TrackPTTK() {
        super();
        pointsDown = 0;
        pointsUp = 0;
        validFrom = new Date();
        validTo = new Date();
    }

    public TrackPTTK(int id, TrackPoint pointA, TrackPoint pointB, List<TrackColor> trackColors, int pointsUp, int pointsDown, Date validFrom) {
        super(id, pointA, pointB, trackColors);
        this.pointsUp = pointsUp;
        this.pointsDown = pointsDown;
        this.validFrom = validFrom;
    }

    public TrackPTTK(int id, TrackPoint pointA, TrackPoint pointB, List<TrackColor> trackColors, int pointsUp, int pointsDown, Date validFrom, Date validTo) {
        super(id, pointA, pointB, trackColors);
        this.pointsUp = pointsUp;
        this.pointsDown = pointsDown;
        this.validFrom = validFrom;
        this.validTo = validTo;
    }

    public TrackPTTK(int id, String name, TrackPoint pointA, TrackPoint pointB, List<TrackColor> trackColors, int pointsUp, int pointsDown, Date validFrom) {
        super(id, name, pointA, pointB, trackColors);
        this.pointsUp = pointsUp;
        this.pointsDown = pointsDown;
        this.validFrom = validFrom;
    }

    public TrackPTTK(int id, String name, TrackPoint pointA, TrackPoint pointB, List<TrackColor> trackColors, int pointsUp, int pointsDown, Date validFrom, Date validTo) {
        super(id, name, pointA, pointB, trackColors);
        this.pointsUp = pointsUp;
        this.pointsDown = pointsDown;
        this.validFrom = validFrom;
        this.validTo = validTo;
    }

    /**
     * Metoda zwracająca pierwszy podtytuł elementu znajdującego się na liście.
     */
    public String getFirstSubsectionForDetails() {
        return pointsUp + "/" + pointsDown;
    }

    public int getPointsUp() {
        return pointsUp;
    }

    public void setPointsUp(int pointsUp) {
        this.pointsUp = pointsUp;
    }

    public int getPointsDown() {
        return pointsDown;
    }

    public void setPointsDown(int pointsDown) {
        this.pointsDown = pointsDown;
    }

    public Date getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Date validFrom) {
        this.validFrom = validFrom;
    }

    public Date getValidTo() {
        return validTo;
    }

    public void setValidTo(Date validTo) {
        this.validTo = validTo;
    }
}
