package sbtwski.gotpttkmanager.model.tracks;

import java.io.Serializable;

/**
 * Klasa używana do przechowywania informacji dotyczących współrzędnych geograficznych punktu trasy.
 */

public class PointCoordinates implements Serializable {
    private int id;
    private int degrees;
    private int minutes;
    private float seconds;

    public PointCoordinates() { }

    public PointCoordinates(int id, int degrees, int minutes, float seconds) {
        this.degrees = degrees;
        this.minutes = minutes;
        this.id = id;

        this.seconds = seconds;
    }

    public int getDegrees() {
        return degrees;
    }

    public void setDegrees(int degrees) {
        this.degrees = degrees;
    }

    public int getMinutes() {
        return minutes;
    }

    public void setMinutes(int minutes) {
        this.minutes = minutes;
    }

    public float getSeconds() {
        return seconds;
    }

    public void setSeconds(float seconds) {
        this.seconds = seconds;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
