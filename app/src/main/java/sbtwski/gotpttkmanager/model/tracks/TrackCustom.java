package sbtwski.gotpttkmanager.model.tracks;

import java.io.Serializable;
import java.util.List;

/**
 * Klasa używana do przechowywania informacji dotyczących trasy własnej.
 */

public class TrackCustom extends Track implements Serializable {
    private int points;

    public TrackCustom() { }

    public TrackCustom(int id, TrackPoint pointA, TrackPoint pointB, List<TrackColor> trackColors, int points) {
        super(id, pointA, pointB, trackColors);
        this.points = points;
    }

    public TrackCustom(int id, String name, TrackPoint pointA, TrackPoint pointB, List<TrackColor> trackColors, int points) {
        super(id, name, pointA, pointB, trackColors);
        this.points = points;
    }


    int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }
}
