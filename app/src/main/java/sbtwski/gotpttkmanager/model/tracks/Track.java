package sbtwski.gotpttkmanager.model.tracks;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import sbtwski.gotpttkmanager.model.RecyclerCompatible;

/**
 * Klasa abstrakcyjna używana do przechowywania podstawowych informacji dotyczących trasy.
 */

@JsonTypeInfo(use=JsonTypeInfo.Id.NAME, include=JsonTypeInfo.As.PROPERTY)
@JsonSubTypes({
        @JsonSubTypes.Type(value = TrackPTTK.class, name = "TrackPTTK"),
        @JsonSubTypes.Type(value = TrackCustom.class, name = "TrackCustom")
})

abstract public class Track implements Serializable, RecyclerCompatible {
    private int id;
    private String name;
    private TrackPoint pointA, pointB;
    private List<TrackColor> trackColors;

    Track() {
        pointA = new TrackPoint();
        pointB = new TrackPoint();
        trackColors = new ArrayList<>();
    }

    Track(int id, TrackPoint pointA, TrackPoint pointB, List<TrackColor> trackColors) {
        this.id = id;
        this.pointA = pointA;
        this.pointB = pointB;
        this.trackColors = trackColors;
    }

    Track(int id, String name, TrackPoint pointA, TrackPoint pointB, List<TrackColor> trackColors) {
        this.id = id;
        this.name = name;
        this.pointA = pointA;
        this.pointB = pointB;
        this.trackColors = trackColors;
    }

    /**
     * Metoda zwracająca tytuł elementu znajdującego się na liście.
     */
    @Override
    public String getItemTitle() {
        return name;
    }

    /**
     * Metoda zwracająca pierwszy podtytuł elementu znajdującego się na liście.
     */
    @Override
    public String getFirstSubsection() {
        return pointA.getName();
    }

    /**
     * Metoda zwracająca drugi podtytuł elementu znajdującego się na liście.
     */
    @Override
    public String getSecondSubsection() {
        return pointB.getName();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public TrackPoint getPointA() {
        return pointA;
    }

    public void setPointA(TrackPoint pointA) {
        this.pointA = pointA;
    }

    public TrackPoint getPointB() {
        return pointB;
    }

    public void setPointB(TrackPoint pointB) {
        this.pointB = pointB;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<TrackColor> getTrackColors() {
        return trackColors;
    }

    public void setTrackColors(List<TrackColor> trackColors) {
        this.trackColors = trackColors;
    }
}
