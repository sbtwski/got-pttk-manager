package sbtwski.gotpttkmanager.model.tracks;

import java.io.Serializable;

import sbtwski.gotpttkmanager.model.data.Country;
import sbtwski.gotpttkmanager.model.data.MountainGroup;
import sbtwski.gotpttkmanager.model.RecyclerCompatible;
import sbtwski.gotpttkmanager.model.SpinnerCompatible;

/**
 * Klasa używana do przechowywania informacji dotyczących punktu trasy.
 */

public class TrackPoint implements Serializable, SpinnerCompatible, RecyclerCompatible {
    private int id, altitude;
    private String name;
    private PointCoordinates latitude, longitude;
    private MountainGroup mountainGroup;
    private Country country;

    public TrackPoint() {
        latitude = new PointCoordinates();
        longitude = new PointCoordinates();
        mountainGroup = new MountainGroup();
        country = new Country();
    }

    public TrackPoint(int id, String name, PointCoordinates latitude, PointCoordinates longitude, int altitude,  MountainGroup mountainGroup, Country country) {
        this.id = id;
        this.altitude = altitude;
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;
        this.mountainGroup = mountainGroup;
        this.country = country;
    }

    public TrackPoint(int id, PointCoordinates latitude, PointCoordinates longitude, int altitude, MountainGroup mountainGroup, Country country) {
        this.id = id;
        this.altitude = altitude;
        this.latitude = latitude;
        this.longitude = longitude;
        this.mountainGroup = mountainGroup;
        this.country = country;
    }

    /**
     * Metoda zwracająca tytuł elementu znajdującego się na liście.
     */
    @Override
    public String getItemTitle() {
        return name;
    }

    /**
     * Metoda zwracająca pierwszy podtytuł elementu znajdującego się na liście.
     */
    @Override
    public String getFirstSubsection() {
        return "";
    }

    /**
     * Metoda zwracająca drugi podtytuł elementu znajdującego się na liście.
     */
    @Override
    public String getSecondSubsection() {
        return "";
    }

    /**
     * Metoda zwracająca nazwę elementu do wyboru z listy rozwijanej.
     */
    @Override
    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAltitude() {
        return altitude;
    }

    public void setAltitude(int altitude) {
        this.altitude = altitude;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PointCoordinates getLatitude() {
        return latitude;
    }

    public void setLatitude(PointCoordinates latitude) {
        this.latitude = latitude;
    }

    public PointCoordinates getLongitude() {
        return longitude;
    }

    public void setLongitude(PointCoordinates longitude) {
        this.longitude = longitude;
    }

    public MountainGroup getMountainGroup() {
        return mountainGroup;
    }

    public void setMountainGroup(MountainGroup mountainGroup) {
        this.mountainGroup = mountainGroup;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public String getAltitudeText() {
        return altitude+"";
    }

    public String getIdText() { return id+""; }
}
