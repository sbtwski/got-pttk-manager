package sbtwski.gotpttkmanager.model.tracks;

import java.io.Serializable;

/**
 * Klasa używana do przechowywania informacji dotyczących koloru szlaku przez który przebiega trasa.
 */

public class TrackColor implements Serializable {
    private int id;
    private String hexColor;

    public TrackColor() { }

    public TrackColor(int id, String hexColor) {
        this.id = id;
        this.hexColor = hexColor;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getHexColor() {
        return hexColor;
    }

    public void setHexColor(String hexColor) {
        this.hexColor = hexColor;
    }
}
