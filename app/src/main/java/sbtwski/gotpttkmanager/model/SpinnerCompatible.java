package sbtwski.gotpttkmanager.model;

/**
 * Interfejs, którego implementacja zapewnia poprawne wyświetlanie elementów na liście rozwijanej.
 */

public interface SpinnerCompatible {
    String getName();
}
