package sbtwski.gotpttkmanager.model.data;

import java.io.Serializable;

/**
 * Klasa używana do przechowywania informacji dotyczących województwa, w którym mieszka turysta lub, w którym znajduje się Referat Weryfikacyjny.
 */

public class Province implements Serializable {
    private int id;
    private String name;

    public Province() { }

    public Province(String name) {
        this.name = name;
    }

    public Province(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
