package sbtwski.gotpttkmanager.model.data;

import java.io.Serializable;

/**
 * Klasa używana do przechowywania informacji dotyczących rodzaju GOT PTTK.
 */

public class BadgeType implements Serializable {
    private int id;
    private String name;

    public BadgeType() { }

    public BadgeType(String name) {
        this.name = name;
    }

    public BadgeType(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
