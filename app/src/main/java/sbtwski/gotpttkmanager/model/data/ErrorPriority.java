package sbtwski.gotpttkmanager.model.data;

import java.io.Serializable;
/**
 * Klasa używana do przechowywania informacji dotyczących priorytetu zgłaszanego przez użytkownika błedu w trasie.
 */


public class ErrorPriority implements Serializable {
    private int id, priorityLevel;

    public ErrorPriority() { }

    public ErrorPriority(int id, int priorityLevel) {
        this.id = id;
        this.priorityLevel = priorityLevel;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPriorityLevel() {
        return priorityLevel;
    }

    public void setPriorityLevel(int priorityLevel) {
        this.priorityLevel = priorityLevel;
    }
}
