package sbtwski.gotpttkmanager.model.data;

/**
 * Klasa służąca do przechowywania stałych używanych w wielu miejscach w aplikacji.
 */

public class Constants {
    /**
     * Adres IP serwera obsługującego komunikację z bazą danych.
     */
    public static final String SERVER_IP = "http://192.168.0.105/laravel/public/api/";
    /**
     * Okres czasu w milisekundach przez który aplikacja ma próbować nawiązać połączenie z bazą danych.
     */
    public static final int ATTEMPT_TIME = 6000;

    public static final String MOUNTAIN_GROUPS_ROUTE = "mountaingroups";
    public static final String POINTS_BY_GROUP_ID_ROUTE = "points/id/";
    public static final String TRACKS_BY_POINT_ID_ROUTE = "tracks/point/";
    public static final String MOUNTAIN_GROUPS_WITH_POINT_CONSTRAINTS_ROUTE = "points/";
    public static final String TRACKS_BY_NAME_ROUTE = "tracks/";
    public static final String TRACKS_BY_ID_ROUTE = "tracks/id/";
    public static final String TOURISTS_BY_NAME_ROUTE = "tourists/";
    public static final String TOURISTS_BY_ID_ROUTE = "tourists/id/";

    public static final int NOTHING_SELECTED = 0;
    public static final int LOWEST_SELECTED = 6;
    public static final int LOW_SELECTED = 3;
    public static final int MEDIUM_SELECTED = 2;
    public static final int HIGH_SELECTED = 1;
    public static final int HIGHEST_SELECTED = 7;

    public static final int GROUP_SPINNER_UPDATE = 0;
    public static final int POINT_A_SPINNER_UPDATE = 1;
    public static final int POINT_B_SPINNER_UPDATE = 2;

    public static final int POINT_A_ADD_REQUEST = 1;
    public static final int POINT_B_ADD_REQUEST = 2;

    public static final int SPINNERS_TO_UPDATE = 4;
    public static final String[] TRACK_COLORS = {"#000000", "#e50000", "#0000e5", "#006400", "#efdb00"};
}
