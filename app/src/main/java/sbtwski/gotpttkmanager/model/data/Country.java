package sbtwski.gotpttkmanager.model.data;

import java.io.Serializable;

/**
 * Klasa używana do przechowywania informacji dotyczących państwa na terenie którego znajduje się punkt trasy.
 */

public class Country implements Serializable {
    private int id;
    private String name;

    public Country() { }

    public Country(String name){
        this.name = name;
    }

    public Country(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
