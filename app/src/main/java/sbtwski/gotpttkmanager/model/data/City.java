package sbtwski.gotpttkmanager.model.data;

import java.io.Serializable;

/**
 * Klasa używana do przechowywania informacji dotyczących miasta zamieszkania turysty lub miasta, w którym znajduje się Referat Weryfikacyjny.
 */

public class City implements Serializable {
    private int id;
    private String name;

    public City() { }

    public City(String name) {
        this.name = name;
    }

    public City(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
