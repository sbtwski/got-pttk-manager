package sbtwski.gotpttkmanager.model.data;

import java.io.Serializable;

/**
 * Klasa używana do przechowywania informacji dotyczących stopnia GOT PTTK.
 */
public class BadgeLevel implements Serializable {
    private int id;
    private String name;

    public BadgeLevel() { }

    public BadgeLevel(String name) {
        this.name = name;
    }

    public BadgeLevel(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
