package sbtwski.gotpttkmanager.model.data;

import java.io.Serializable;

import sbtwski.gotpttkmanager.model.RecyclerCompatible;
import sbtwski.gotpttkmanager.model.SpinnerCompatible;

/**
 * Klasa używana do przechowywania informacji dotyczących grupy górskiej, w której znajduje się punkt trasy.
 */

public class MountainGroup implements Serializable, SpinnerCompatible, RecyclerCompatible {
    private int id;
    private String name;

    public MountainGroup(){ }

    public MountainGroup(String name) {
        this.name = name;
    }

    public MountainGroup(int id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public String getItemTitle() {
        return name;
    }

    @Override
    public String getFirstSubsection() {
        return "";
    }

    @Override
    public String getSecondSubsection() {
        return "";
    }

    @Override
    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIdText() { return id+""; }
}
