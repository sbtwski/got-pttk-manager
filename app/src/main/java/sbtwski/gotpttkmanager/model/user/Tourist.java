package sbtwski.gotpttkmanager.model.user;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import sbtwski.gotpttkmanager.model.RecyclerCompatible;
import sbtwski.gotpttkmanager.model.data.City;
import sbtwski.gotpttkmanager.model.data.Province;

/**
 * Klasa używana do przechowywania informacji dotyczących turysty.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class Tourist implements Serializable, RecyclerCompatible {
    private Date birthDate;
    private int points;
    private int identityCardNumber;
    private int id;
    private boolean isDisabled;
    private boolean isForeman;
    private String name, surname, PESEL, phoneNumber;
    private City residenceCity;
    private Province residenceProvince;
    private Badge latestBadge;
    private List<Trip> trips;

    public Tourist(){ }

    public Tourist(int id, Date birthDate, int points, int identityCardNumber, boolean isDisabled, boolean isForeman, String name,
                   String surname, String PESEL, String phoneNumber, City residenceCity, Province residenceProvince, Badge latestBadge, List<Trip> trips) {
        this.birthDate = birthDate;
        this.points = points;
        this.identityCardNumber = identityCardNumber;
        this.id = id;
        this.isDisabled = isDisabled;
        this.isForeman = isForeman;
        this.name = name;
        this.surname = surname;
        this.PESEL = PESEL;
        this.phoneNumber = phoneNumber;
        this.residenceCity = residenceCity;
        this.residenceProvince = residenceProvince;
        this.latestBadge = latestBadge;
        this.trips = trips;
    }

    /**
     * Metoda zwracająca tytuł elementu znajdującego się na liście.
     */
    @Override
    public String getItemTitle() {
        return name + " " + surname;
    }

    /**
     * Metoda zwracająca pierwszy podtytuł elementu znajdującego się na liście.
     */
    @Override
    public String getFirstSubsection() {
        return id + "";
    }

    /**
     * Metoda zwracająca drugi podtytuł elementu znajdującego się na liście.
     */
    @Override
    public String getSecondSubsection() {
        return  latestBadge.getType().getName() + " " + latestBadge.getLevel().getName();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public int getIdentityCardNumber() {
        return identityCardNumber;
    }

    public void setIdentityCardNumber(int identityCardNumber) {
        this.identityCardNumber = identityCardNumber;
    }

    public boolean isDisabled() {
        return isDisabled;
    }

    public void setIsDisabled(boolean isDisabled) {
        this.isDisabled = isDisabled;
    }

    public boolean isForeman() {
        return isForeman;
    }

    public void setIsForeman(boolean isForeman) {
        this.isForeman = isForeman;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPESEL() {
        return PESEL;
    }

    public void setPESEL(String PESEL) {
        this.PESEL = PESEL;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public City getResidenceCity() {
        return residenceCity;
    }

    public void setResidenceCity(City residenceCity) {
        this.residenceCity = residenceCity;
    }

    public Province getResidenceProvince() {
        return residenceProvince;
    }

    public void setResidenceProvince(Province residenceProvince) {
        this.residenceProvince = residenceProvince;
    }

    public Badge getLatestBadge() {
        return latestBadge;
    }

    public void setLatestBadge(Badge latestBadge) {
        this.latestBadge = latestBadge;
    }

    public List<Trip> getTrips() {
        return trips;
    }

    public void setTrips(List<Trip> trips) {
        this.trips = trips;
    }
}
