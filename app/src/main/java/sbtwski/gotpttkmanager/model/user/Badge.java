package sbtwski.gotpttkmanager.model.user;

import java.io.Serializable;
import java.util.Date;

import sbtwski.gotpttkmanager.model.data.BadgeLevel;
import sbtwski.gotpttkmanager.model.data.BadgeType;

/**
 * Klasa używana do przechowywania informacji dotyczących GOT PTTK.
 */

public class Badge implements Serializable {
    private Date acquisitionDate;
    private Date startDate;
    private int pointsRequired, specialNorms, id;
    private BadgeLevel level;
    private BadgeType type;

    public Badge(){ }

    public Badge(int id, Date startDate, int pointsRequired, int specialNorms, BadgeLevel level, BadgeType type) {
        this.id = id;
        this.startDate = startDate;
        this.pointsRequired = pointsRequired;
        this.specialNorms = specialNorms;
        this.level = level;
        this.type = type;
    }

    public Badge(int id, Date acquisitionDate, Date startDate, int pointsRequired, int specialNorms, BadgeLevel level, BadgeType type) {
        this.id = id;
        this.acquisitionDate = acquisitionDate;
        this.startDate = startDate;
        this.pointsRequired = pointsRequired;
        this.specialNorms = specialNorms;
        this.level = level;
        this.type = type;
    }

    public Badge(int id, Date acquisitionDate, Date startDate, int pointsRequired, BadgeLevel level, BadgeType type) {
        this.id = id;
        this.acquisitionDate = acquisitionDate;
        this.startDate = startDate;
        this.pointsRequired = pointsRequired;
        this.level = level;
        this.type = type;
    }

    public Badge(int id, Date startDate, int pointsRequired, BadgeLevel level, BadgeType type) {
        this.id = id;
        this.startDate = startDate;
        this.pointsRequired = pointsRequired;
        this.level = level;
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getAcquisitionDate() {
        return acquisitionDate;
    }

    public void setAcquisitionDate(Date acquisitionDate) {
        this.acquisitionDate = acquisitionDate;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public int getPointsRequired() {
        return pointsRequired;
    }

    public void setPointsRequired(int pointsRequired) {
        this.pointsRequired = pointsRequired;
    }

    BadgeLevel getLevel() {
        return level;
    }

    public void setLevel(BadgeLevel level) {
        this.level = level;
    }

    BadgeType getType() {
        return type;
    }

    public void setType(BadgeType type) {
        this.type = type;
    }

    public int getSpecialNorms() {
        return specialNorms;
    }

    public void setSpecialNorms(int specialNorms) {
        this.specialNorms = specialNorms;
    }
}
