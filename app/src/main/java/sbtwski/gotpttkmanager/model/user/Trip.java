package sbtwski.gotpttkmanager.model.user;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import sbtwski.gotpttkmanager.model.RecyclerCompatible;
import sbtwski.gotpttkmanager.model.tracks.TrackFromTrip;

/**
 * Klasa używana do przechowywania informacji dotyczących wycieczki.
 */

public class Trip implements Serializable, RecyclerCompatible {
    private Date startDate;
    private Date endDate;
    private String name;
    private int points, id;
    private List<TrackFromTrip> tracks;

    public Trip(){ }

    public Trip(int id, Date startDate, Date endDate, String name, int points, List<TrackFromTrip> tracks) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.name = name;
        this.points = points;
        this.id = id;
        this.tracks = tracks;
    }

    /**
     * Metoda zwracająca tytuł elementu znajdującego się na liście.
     */
    @Override
    public String getItemTitle() {
        return name;
    }

    /**
     * Metoda zwracająca pierwszy podtytuł elementu znajdującego się na liście.
     */
    @Override
    public String getFirstSubsection() {
        DateFormat formatter = new SimpleDateFormat("dd.MM.yyyy", Locale.UK);
        return formatter.format(startDate);
    }

    /**
     * Metoda zwracająca drugi podtytuł elementu znajdującego się na liście.
     */
    @Override
    public String getSecondSubsection() {
        DateFormat formatter = new SimpleDateFormat("dd.MM.yyyy", Locale.UK);
        return formatter.format(endDate);
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<TrackFromTrip> getTracks() {
        return tracks;
    }

    public void setTracks(List<TrackFromTrip> tracks) {
        this.tracks = tracks;
    }

    public List<? extends RecyclerCompatible> getTracksForRecycler() {
        return tracks;
    }

    public String getIdText() {
        return id+"";
    }

    public String getPointsText() {
        return points+"";
    }
}
