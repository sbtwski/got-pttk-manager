package sbtwski.gotpttkmanager.model.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.util.Date;

import sbtwski.gotpttkmanager.model.data.ErrorPriority;
import sbtwski.gotpttkmanager.model.tracks.PointCoordinates;

/**
 * Klasa używana do przechowywania informacji dotyczących błędu na trasie zgłaszanego przez użytkownika.
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
public class TrackError implements Serializable {
    private int id;
    private Date filingDate;
    @JsonIgnore()
    private String evidence;
    private String description;
    private int declarant;
    private int priority;
    private PointCoordinates latitude, longitude;

    public TrackError(int id, Date filingDate, String evidence, String description, Tourist declarant, ErrorPriority priority, PointCoordinates latitude, PointCoordinates longitude) {
        this.id = id;
        this.filingDate = filingDate;
        this.evidence = evidence;
        this.description = description;
        this.declarant = declarant.getId();
        this.priority = priority.getPriorityLevel();
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public TrackError(int id, Date filingDate, String evidence, String description, Tourist declarant, ErrorPriority priority) {
        this.id = id;
        this.filingDate = filingDate;
        this.evidence = evidence;
        this.description = description;
        this.declarant = declarant.getId();
        this.priority = priority.getPriorityLevel();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getFilingDate() {
        return filingDate;
    }

    public void setFilingDate(Date filingDate) {
        this.filingDate = filingDate;
    }

    public String getEvidence() {
        return evidence;
    }

    public void setEvidence(String evidence) {
        this.evidence = evidence;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getDeclarant() {
        return declarant;
    }

    public void setDeclarant(int declarant) {
        this.declarant = declarant;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public PointCoordinates getLatitude() {
        return latitude;
    }

    public void setLatitude(PointCoordinates latitude) {
        this.latitude = latitude;
    }

    public PointCoordinates getLongitude() {
        return longitude;
    }

    public void setLongitude(PointCoordinates longitude) {
        this.longitude = longitude;
    }
}
