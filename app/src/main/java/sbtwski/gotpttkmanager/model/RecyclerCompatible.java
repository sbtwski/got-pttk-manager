package sbtwski.gotpttkmanager.model;

/**
 * Interfejs, którego implementacja zapewnia poprawne wyświetlanie elementów na liście.
 */

public interface RecyclerCompatible {
    String getItemTitle();
    String getFirstSubsection();
    String getSecondSubsection();
}
