package sbtwski.gotpttkmanager.presenter.versatilityItems;

import java.util.ArrayList;

import sbtwski.gotpttkmanager.model.SpinnerCompatible;

/**
 * Wątek obsługujący dodawanie danych do listy rozwijanej po ich pobraniu.
 */

public class SpinnerFetchRunnable implements Runnable{
    private android.os.Handler fetchHandler;
    private SpinnerFetchingActivity activity;
    private int updateCode;

    public SpinnerFetchRunnable(android.os.Handler fetchHandler, SpinnerFetchingActivity activity, int updateCode) {
        this.fetchHandler = fetchHandler;
        this.activity = activity;
        this.updateCode = updateCode;
    }

    @Override
    public void run() {
        if(RequestDataManager.requestCounter == 0) {
            ArrayList<SpinnerCompatible> fetchedData;
            fetchHandler.removeCallbacks(this);

            RequestDataManager rdm = RequestDataManager.getInstance();
            fetchedData = rdm.getActualSpinnerDataSaved();
            activity.setupSpinner(fetchedData, updateCode);
        }
        else{
            fetchHandler.postDelayed(this, 100);
        }
    }
}

