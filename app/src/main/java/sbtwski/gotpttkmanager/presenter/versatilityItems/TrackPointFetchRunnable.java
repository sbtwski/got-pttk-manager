package sbtwski.gotpttkmanager.presenter.versatilityItems;

import android.widget.TextView;

import java.util.ArrayList;

import sbtwski.gotpttkmanager.model.RecyclerCompatible;

/**
 * Wątek obsługujący dodawanie danych do listy tras dostępnych z punktu po ich pobraniu.
 */

public class TrackPointFetchRunnable implements Runnable{
    private TrackPointDetailsAdapter adapter;
    private android.os.Handler fetchHandler;
    private TextView databaseSize;

    public TrackPointFetchRunnable(TrackPointDetailsAdapter adapter, TextView databaseSize, android.os.Handler fetchHandler) {
        this.adapter = adapter;
        this.fetchHandler = fetchHandler;
        this.databaseSize = databaseSize;
    }

    @Override
    public void run() {
        if(RequestDataManager.requestCounter == 0) {
            ArrayList<RecyclerCompatible> fetchedData;
            fetchHandler.removeCallbacks(this);

            RequestDataManager rdm = RequestDataManager.getInstance();
            fetchedData = rdm.getActualRecyclerDataSaved();

            adapter.addDatabase(fetchedData);
            String sizeText = fetchedData.size() + "";
            databaseSize.setText(sizeText);
        }
        else{
            fetchHandler.postDelayed(this, 100);
        }
    }
}
