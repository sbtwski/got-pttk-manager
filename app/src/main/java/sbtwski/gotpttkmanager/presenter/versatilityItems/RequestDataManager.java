package sbtwski.gotpttkmanager.presenter.versatilityItems;

import java.util.ArrayList;

import sbtwski.gotpttkmanager.model.RecyclerCompatible;
import sbtwski.gotpttkmanager.model.SpinnerCompatible;

/**
 * Singleton wspomagający obsługę pobierania danych z serwera.
 */

public class RequestDataManager {
    private static RequestDataManager rdmInstance;
    private ArrayList<RecyclerCompatible> actualRecyclerDataSaved;
    private ArrayList<SpinnerCompatible> actualSpinnerDataSaved;
    public static int requestCounter;

    private RequestDataManager(){
        actualRecyclerDataSaved = new ArrayList<>();
        actualSpinnerDataSaved = new ArrayList<>();
        requestCounter = 0;
    }

    public static synchronized RequestDataManager getInstance(){
        if(rdmInstance == null){
            rdmInstance = new RequestDataManager();
        }
        return rdmInstance;
    }

    ArrayList<RecyclerCompatible> getActualRecyclerDataSaved() {
        return actualRecyclerDataSaved;
    }

    void setActualRecyclerDataSaved(ArrayList<RecyclerCompatible> actualRecyclerDataSaved) {
        this.actualRecyclerDataSaved = actualRecyclerDataSaved;
    }

    ArrayList<SpinnerCompatible> getActualSpinnerDataSaved() {
        return actualSpinnerDataSaved;
    }

    public void setActualSpinnerDataSaved(ArrayList<SpinnerCompatible> actualSpinnerDataSaved) {
        this.actualSpinnerDataSaved = actualSpinnerDataSaved;
    }
}

