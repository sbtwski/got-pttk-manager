package sbtwski.gotpttkmanager.presenter.versatilityItems;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import sbtwski.gotpttkmanager.R;
import sbtwski.gotpttkmanager.model.RecyclerCompatible;
import sbtwski.gotpttkmanager.presenter.RecyclerViewClickListener;

/**
 * Adapter do listy elementów podstawowych.
 */

public class BasicSelectionAdapter extends RecyclerView.Adapter<BasicSelectionAdapter.BasicSelectionHolder> {
    private List database;
    private RecyclerViewClickListener clickListener;

    class BasicSelectionHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private RecyclerViewClickListener holderListener;
        TextView itemTitle;

        BasicSelectionHolder(View itemView, RecyclerViewClickListener listener) {
            super(itemView);

            itemTitle = itemView.findViewById(R.id.basic_selection_item_title);
            holderListener = listener;

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            holderListener.onClick(view, getAdapterPosition());
        }
    }

    public BasicSelectionAdapter(RecyclerViewClickListener listener) {
        clickListener = listener;
        database = new ArrayList<>();
    }

    @Override
    public void onBindViewHolder(@NonNull BasicSelectionHolder basicHolder, int i) {
        TextView itemTitle = basicHolder.itemTitle;
        RecyclerCompatible toBind = (RecyclerCompatible)database.get(i);
        itemTitle.setText(toBind.getItemTitle());
    }


    @Override
    @NonNull
    public BasicSelectionHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.basic_selection_item, parent, false);
        return new BasicSelectionHolder(view, clickListener);
    }

    @Override
    public int getItemCount() {
        return database.size();
    }

    void addDatabase(List database) {
        this.database = database;
        notifyDataSetChanged();
    }

    public RecyclerCompatible getItem(int position) {
        return position < database.size() ? (RecyclerCompatible)database.get(position) : null;
    }
}
