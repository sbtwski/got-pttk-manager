package sbtwski.gotpttkmanager.presenter.versatilityItems;

import java.util.ArrayList;

import sbtwski.gotpttkmanager.model.RecyclerCompatible;

/**
 * Wątek obsługujący dodawanie danych do listy po ich pobraniu.
 */

public class BasicFetchRunnable implements Runnable{
    private BasicSelectionAdapter adapter;
    private android.os.Handler fetchHandler;

    public BasicFetchRunnable(BasicSelectionAdapter adapter, android.os.Handler fetchHandler) {
        this.adapter = adapter;
        this.fetchHandler = fetchHandler;
    }

    @Override
    public void run() {
        if(RequestDataManager.requestCounter == 0) {
            ArrayList<RecyclerCompatible> fetchedData;
            fetchHandler.removeCallbacks(this);

            RequestDataManager rdm = RequestDataManager.getInstance();
            fetchedData = rdm.getActualRecyclerDataSaved();

            adapter.addDatabase(fetchedData);
        }
        else{
            fetchHandler.postDelayed(this, 100);
        }
    }
}
