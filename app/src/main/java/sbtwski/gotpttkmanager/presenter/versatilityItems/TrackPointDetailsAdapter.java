package sbtwski.gotpttkmanager.presenter.versatilityItems;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import sbtwski.gotpttkmanager.R;
import sbtwski.gotpttkmanager.model.RecyclerCompatible;
import sbtwski.gotpttkmanager.model.data.Constants;
import sbtwski.gotpttkmanager.model.tracks.TrackColor;
import sbtwski.gotpttkmanager.model.tracks.TrackPTTK;

/**
 * Adapter do listy tras których częścią jest wybrany punkt.
 */

public class TrackPointDetailsAdapter extends RecyclerView.Adapter<TrackPointDetailsAdapter.TrackPointDetailsHolder> {
    private List database;

    class TrackPointDetailsHolder extends RecyclerView.ViewHolder{
        TextView itemTitle, itemSubsection;
        LinearLayout trackColors;

        TrackPointDetailsHolder(View itemView) {
            super(itemView);
            itemTitle = itemView.findViewById(R.id.track_point_details_item_title);
            itemSubsection = itemView.findViewById(R.id.track_point_details_item_subsection_value);
            trackColors = itemView.findViewById(R.id.track_point_details_item_color_container);
        }
    }

    public TrackPointDetailsAdapter() {
        database = new ArrayList<>();
    }

    @Override
    public void onBindViewHolder(@NonNull TrackPointDetailsHolder trackPointHolder, int i) {
        TextView itemTitle = trackPointHolder.itemTitle;
        TextView itemSubsection = trackPointHolder.itemSubsection;
        LinearLayout trackColors = trackPointHolder.trackColors;

        RecyclerCompatible toBind = (RecyclerCompatible)database.get(i);
        itemTitle.setText(toBind.getItemTitle());
        itemSubsection.setText(((TrackPTTK)toBind).getFirstSubsectionForDetails());

        List<TrackColor> colorsList = generateColors();
        for(TrackColor color: colorsList)
            colorsInflater(color.getHexColor(), trackColors);
    }


    @Override
    @NonNull
    public TrackPointDetailsHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.track_point_details_item, parent, false);
        return new TrackPointDetailsHolder(view);
    }

    @Override
    public int getItemCount() {
        return database.size();
    }

    void addDatabase(List database) {
        this.database = database;
        notifyDataSetChanged();
    }

    public RecyclerCompatible getItem(int position) {
        return position < database.size() ? (RecyclerCompatible)database.get(position) : null;
    }

    private void colorsInflater(String hexColor, LinearLayout colorsContainer) {
        LayoutInflater inflater = (LayoutInflater) colorsContainer.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View colorView = inflater.inflate(R.layout.track_color_item, colorsContainer, false);
        TextView colorPart = colorView.findViewById(R.id.track_color_part);

        colorPart.setBackgroundColor(Color.parseColor(hexColor));
        colorsContainer.addView(colorView);
    }

    private List<TrackColor> generateColors() {
        Random generator = new Random();
        int amount = generator.nextInt(3);

        List<TrackColor> colors = new ArrayList<>();

        if(amount > 0){
            int length = Constants.TRACK_COLORS.length;
            int first = generator.nextInt(length);
            colors.add(new TrackColor(0, Constants.TRACK_COLORS[first]));

            if(amount > 1){
                int second = (first + generator.nextInt(length-1) + 1)%length;
                colors.add(new TrackColor(1, Constants.TRACK_COLORS[second]));
            }
        }

        return colors;
    }
}
