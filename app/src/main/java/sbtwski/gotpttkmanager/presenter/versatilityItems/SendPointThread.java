package sbtwski.gotpttkmanager.presenter.versatilityItems;

import android.content.Context;
import android.widget.Toast;

import com.loopj.android.http.RequestParams;
import com.loopj.android.http.SyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import cz.msebera.android.httpclient.Header;
import sbtwski.gotpttkmanager.R;
import sbtwski.gotpttkmanager.model.tracks.TrackPoint;

/**
 * Wątek służący do wysyłania nowego punktu na serwer.
 */

public class SendPointThread implements Runnable {
    private String url;
    private TrackPoint point;
    private Context context;

    public SendPointThread(TrackPoint point, String url, Context context){
        this.point = point;
        this.url = url;
        this.context = context;
    }

    private void sendRequest(TrackPoint newPoint, String url){
        RequestDataManager.requestCounter = 1;
        SyncHttpClient client = new SyncHttpClient();
        client.setMaxRetriesAndTimeout(5, 10000);

        RequestParams params = new RequestParams();
        params.put("name", newPoint.getName());
        params.put("altitude", newPoint.getAltitude());
        params.put("mountainGroup", newPoint.getMountainGroup().getId());

        params.put("latitudedegrees", newPoint.getLatitude().getDegrees());
        params.put("latitudeminutes", newPoint.getLatitude().getMinutes());
        params.put("latitudeseconds", newPoint.getLatitude().getSeconds());

        params.put("longitudedegrees", newPoint.getLongitude().getDegrees());
        params.put("longitudeminutes", newPoint.getLongitude().getMinutes());
        params.put("longitudeseconds", newPoint.getLongitude().getSeconds());

        client.post(url, params, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                if (responseString != null)
                    RequestDataManager.requestCounter = 0;

                Toast.makeText(context, context.getText(R.string.point_add_failed), Toast.LENGTH_LONG).show();
                throwable.fillInStackTrace();

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                if (responseString != null)
                    RequestDataManager.requestCounter = 0;
            }
        });
    }

    @Override
    public void run() {
        sendRequest(point, url);
    }
}

