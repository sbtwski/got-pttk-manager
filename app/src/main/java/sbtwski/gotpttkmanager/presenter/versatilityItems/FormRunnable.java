package sbtwski.gotpttkmanager.presenter.versatilityItems;

import android.content.Context;
import android.content.Intent;

import sbtwski.gotpttkmanager.R;
import sbtwski.gotpttkmanager.presenter.PromptActivity;
import sbtwski.gotpttkmanager.presenter.ReconnectActivity;

/**
 * Wątek obsługujący rezultat przesyłania formularza zgłoszenia błędu na serwer.
 */

public class FormRunnable implements Runnable {
    private android.os.Handler fetchHandler;
    private Context context;
    private long startTime;

    public FormRunnable(android.os.Handler fetchHandler, Context context) {
        this.fetchHandler = fetchHandler;
        this.context = context;
        startTime = System.currentTimeMillis();
    }

    @Override
    public void run() {
        Intent toPrompt;
        if(RequestDataManager.requestCounter == 0) {
            fetchHandler.removeCallbacks(this);
            if (context.getSharedPreferences("success", Context.MODE_PRIVATE).getBoolean("succ", false)) {
                toPrompt = new Intent(context, PromptActivity.class);
                toPrompt.putExtra("success", true);
                toPrompt.putExtra("message", context.getResources().getString(R.string.error_form_success));
            } else {
                toPrompt = new Intent(context, ReconnectActivity.class);
                toPrompt.putExtra("message", context.getResources().getString(R.string.database_reconnect));
            }
            toPrompt.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            toPrompt.putExtra("logged", true);
            context.startActivity(toPrompt);
        }
        else{
            if(System.currentTimeMillis() - startTime > 1000){
                fetchHandler.removeCallbacks(this);
                toPrompt = new Intent(context, ReconnectActivity.class);
                toPrompt.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                toPrompt.putExtra("message", context.getResources().getString(R.string.database_reconnect));
                toPrompt.putExtra("logged", true);
                context.startActivity(toPrompt);
            }
            else
                fetchHandler.postDelayed(this, 100);
        }
    }
}
