package sbtwski.gotpttkmanager.presenter.versatilityItems;

import android.content.Context;
import android.util.Log;

import com.loopj.android.http.RequestParams;
import com.loopj.android.http.SyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import cz.msebera.android.httpclient.Header;
import sbtwski.gotpttkmanager.model.data.Constants;
import sbtwski.gotpttkmanager.model.tracks.TrackPTTK;

/**
 * Wątek służący do wysyłania edytowanej trasy na serwer.
 */

public class SendTrackThread implements Runnable {
    private String url;
    private TrackPTTK edited;
    private Context context;
    private TrackPTTK selectedTrack;

    public SendTrackThread(TrackPTTK edited, String url, Context context, TrackPTTK selectedTrack){
        this.edited = edited;
        this.url = url;
        this.context = context;
        this.selectedTrack = selectedTrack;
    }

    private void sendRequest(TrackPTTK track, String url, TrackPTTK selectedTrack){
        if (selectedTrack != null){
            sendUpdate(selectedTrack);
        }

        SyncHttpClient client = new SyncHttpClient();
        client.setMaxRetriesAndTimeout(1, 10000);

        RequestDataManager.requestCounter = 1;

        RequestParams params = new RequestParams();
        params.put("name", track.getName());
        params.put("pointsUp", track.getPointsUp());
        params.put("pointsDown", track.getPointsDown());
        params.put("pointA", track.getPointA().getId());
        params.put("pointB", track.getPointB().getId());

        client.post(url, params, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                if (responseString != null)
                    RequestDataManager.requestCounter = 0;

                context.getSharedPreferences("success", Context.MODE_PRIVATE).edit()
                        .putBoolean("succ", false)
                        .apply();
                throwable.fillInStackTrace();

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                if (responseString != null)
                    RequestDataManager.requestCounter = 0;

                context.getSharedPreferences("success", Context.MODE_PRIVATE).edit()
                        .putBoolean("succ", true)
                        .apply();
            }
        });
    }
    private void sendUpdate(TrackPTTK selectedTrack){
        SyncHttpClient client = new SyncHttpClient();

        RequestParams params = new RequestParams();
        params.put("id", selectedTrack.getId());
        client.patch(Constants.SERVER_IP + "upTrack", params, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Log.i("updated", "false");
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                Log.i("updated", "true");
            }
        });
    }
    @Override
    public void run() {
        sendRequest(edited, url, selectedTrack);
    }
}
