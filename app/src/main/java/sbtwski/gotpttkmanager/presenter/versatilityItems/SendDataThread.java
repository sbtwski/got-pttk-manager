package sbtwski.gotpttkmanager.presenter.versatilityItems;

import android.content.Context;

import com.loopj.android.http.RequestParams;
import com.loopj.android.http.SyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import java.io.FileNotFoundException;

import cz.msebera.android.httpclient.Header;
import sbtwski.gotpttkmanager.model.user.TrackError;
import sbtwski.gotpttkmanager.presenter.userInputPresenters.ErrorFormActivity;

/**
 * Wątek służący do wysyłania formularza zgłoszenia błędu na serwer.
 */

public class SendDataThread implements Runnable {
    private String url;
    private TrackError err;
    private Context context;

    public SendDataThread(TrackError err, String url, Context context){
        this.err = err;
        this.url = url;
        this.context = context;
    }

    private void sendRequest(TrackError error, String url){
        SyncHttpClient client = new SyncHttpClient();
        client.setMaxRetriesAndTimeout(1, 10000);

        RequestParams params = new RequestParams();
        params.put("priority", error.getPriority());
        params.put("description", error.getDescription());
        params.put("declarant", error.getDeclarant());
        if(error.getLatitude() != null) {
            params.put("latitudedegrees", error.getLatitude().getDegrees());
            params.put("latitudeminutes", error.getLatitude().getMinutes());
            params.put("latitudeseconds", error.getLatitude().getSeconds());
        }
        if(error.getLongitude() != null) {
            params.put("longitudedegrees", error.getLongitude().getDegrees());
            params.put("longitudeminutes", error.getLongitude().getMinutes());
            params.put("longitudeseconds", error.getLongitude().getSeconds());
        }

        if (ErrorFormActivity.evidence != null) {
            try {
                params.put("evidence", ErrorFormActivity.evidence);
            }
            catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        client.post(url, params, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                if (responseString != null)
                    RequestDataManager.requestCounter = 0;
                ErrorFormActivity.evidence = null;
                context.getSharedPreferences("success", Context.MODE_PRIVATE).edit()
                        .putBoolean("succ", false)
                        .apply();
                throwable.fillInStackTrace();

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                if (responseString != null)
                    RequestDataManager.requestCounter = 0;
                ErrorFormActivity.evidence = null;
                context.getSharedPreferences("success", Context.MODE_PRIVATE).edit()
                        .putBoolean("succ", true)
                        .apply();
            }
        });
    }

    @Override
    public void run() {
        sendRequest(err, url);
    }
}

