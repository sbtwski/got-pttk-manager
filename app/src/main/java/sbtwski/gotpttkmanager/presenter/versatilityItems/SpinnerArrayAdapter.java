package sbtwski.gotpttkmanager.presenter.versatilityItems;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.Collection;
import java.util.List;

import sbtwski.gotpttkmanager.R;
import sbtwski.gotpttkmanager.model.SpinnerCompatible;

/**
 * Adapter do listy rozwijanej elementów.
 */

public class SpinnerArrayAdapter extends ArrayAdapter<SpinnerCompatible> {
    private List<SpinnerCompatible> items;
    private Activity activity;

    public SpinnerArrayAdapter(Activity activity, List<SpinnerCompatible> items) {
        super(activity, android.R.layout.simple_list_item_1, items);
        this.items = items;
        this.activity = activity;
    }

    @Override
    public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = activity.getLayoutInflater();
            convertView = inflater.inflate(R.layout.spinner_item_dropdown, parent,false);
        }
        TextView pointName = convertView.findViewById(R.id.spinner_item_dropdown_label);
        pointName.setText(items.get(position).getName());
        return convertView;
    }

    @Override
    public SpinnerCompatible getItem(int position) {
        return items.get(position);
    }

    @Override
    @SuppressWarnings("unchecked")
    public void addAll(@NonNull Collection<? extends SpinnerCompatible> collection) {
        items = (List<SpinnerCompatible>)collection;
    }

    @Override
    @NonNull
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = activity.getLayoutInflater();
            convertView = inflater.inflate(R.layout.spinner_item, parent,false);
        }
        TextView pointName = convertView.findViewById(R.id.spinner_item_label);
        pointName.setText(items.get(position).getName());
        return convertView;
    }
}
