package sbtwski.gotpttkmanager.presenter.versatilityItems;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;

import sbtwski.gotpttkmanager.R;
import sbtwski.gotpttkmanager.model.RecyclerCompatible;
import sbtwski.gotpttkmanager.model.data.Constants;
import sbtwski.gotpttkmanager.presenter.ReconnectActivity;

/**
 * Aktywność bazowa zapewniająca obsługę pobierania danych z serwera do list w przypadku użytkownika niezalogowanego.
 */

public class RecyclerFetchingActivity extends AppCompatActivity {

    public void fetchData(final Intent forActivity, String dataLocation, final Class<? extends RecyclerCompatible> itemsClass) {
        String url = Constants.SERVER_IP + dataLocation;

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        ArrayList<RecyclerCompatible> dataFromJson = new ArrayList<>();
                        ObjectMapper jsonMapper = new ObjectMapper();

                        for(int i=0; i<response.length();i++) {
                            try {
                                RecyclerCompatible current = jsonMapper.readValue(response.getString(i), itemsClass);
                                dataFromJson.add(current);
                            }
                            catch (JSONException e){
                                Log.e("JSON Response","getString failed");
                            }
                            catch (IOException e) {
                                Log.e("JSON Response", "readValue failed");
                            }
                        }

                        RequestDataManager rdm = RequestDataManager.getInstance();
                        rdm.setActualRecyclerDataSaved(dataFromJson);
                        RequestDataManager.requestCounter = 0;
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Intent toReconnect = new Intent(getApplicationContext(), ReconnectActivity.class);
                        toReconnect.putExtra("message", getText(R.string.database_reconnect));
                        startActivity(toReconnect);
                    }
                });

        ConnectionSingleton.getInstance(this).addToRequestQueue(jsonArrayRequest);
        RequestDataManager.requestCounter++;

        startActivity(forActivity);
        overridePendingTransition(R.anim.slide_in_up,R.anim.slide_out_up);
    }
}
