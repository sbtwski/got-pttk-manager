package sbtwski.gotpttkmanager.presenter.versatilityItems;

import android.content.Context;
import android.content.Intent;

import sbtwski.gotpttkmanager.R;
import sbtwski.gotpttkmanager.presenter.PromptActivity;
import sbtwski.gotpttkmanager.presenter.ReconnectActivity;

/**
 * Wątek obsługujący rezultat przesyłania edytowanej trasy na serwer.
 */

public class EditRunnable implements Runnable {
    private android.os.Handler fetchHandler;
    private Context context;

    public EditRunnable(android.os.Handler fetchHandler, Context context) {
        this.fetchHandler = fetchHandler;
        this.context = context;
    }

    @Override
    public void run() {
        Intent toPrompt;
        if(RequestDataManager.requestCounter == 0) {
            fetchHandler.removeCallbacks(this);
            if (context.getSharedPreferences("success", Context.MODE_PRIVATE).getBoolean("succ", false)) {
                toPrompt = new Intent(context, PromptActivity.class);
                toPrompt.putExtra("success", true);
                toPrompt.putExtra("message", context.getResources().getString(R.string.track_edit_transferred_prompt));
            } else {
                toPrompt = new Intent(context, ReconnectActivity.class);
                toPrompt.putExtra("message", context.getResources().getString(R.string.database_reconnect));
            }
            toPrompt.putExtra("logged", true);
            context.startActivity(toPrompt);
        }
        else{
            fetchHandler.postDelayed(this, 100);
        }
    }
}
