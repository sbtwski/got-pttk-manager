package sbtwski.gotpttkmanager.presenter.versatilityItems;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Singleton służący do obsługi kolejki żądań wysyłanych do serwera.
 */

public class ConnectionSingleton {
    private static ConnectionSingleton mInstance;
    private RequestQueue mRequestQueue;
    private Context mCtx;

    private ConnectionSingleton(Context context) {
        mCtx = context;
        mRequestQueue = getRequestQueue();
    }

    public static synchronized ConnectionSingleton getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new ConnectionSingleton(context);
        }
        return mInstance;
    }

    private RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(mCtx.getApplicationContext());
        }
        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req) {
        getRequestQueue().add(req);
    }
}
