package sbtwski.gotpttkmanager.presenter.versatilityItems;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import sbtwski.gotpttkmanager.R;
import sbtwski.gotpttkmanager.model.RecyclerCompatible;
import sbtwski.gotpttkmanager.presenter.RecyclerViewClickListener;

/**
 * Adapter do listy elementów ze szczegółami.
 */

public class ExtendedSelectionAdapter extends RecyclerView.Adapter<ExtendedSelectionAdapter.ExtendedSelectionHolder> {
    private List database;
    private RecyclerViewClickListener clickListener;

    class ExtendedSelectionHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private RecyclerViewClickListener holderListener;
        TextView itemTitle, firstSubsection, secondSubsection;

        ExtendedSelectionHolder(View itemView, RecyclerViewClickListener listener) {
            super(itemView);

            itemTitle = itemView.findViewById(R.id.extended_selection_item_title);
            firstSubsection = itemView.findViewById(R.id.extended_selection_item_subsection_1);
            secondSubsection = itemView.findViewById(R.id.extended_selection_item_subsection_2);
            holderListener = listener;

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            holderListener.onClick(view, getAdapterPosition());
        }
    }

    public ExtendedSelectionAdapter(RecyclerViewClickListener listener) {
        clickListener = listener;
        database = new ArrayList<>();
    }

    @Override
    public void onBindViewHolder(@NonNull ExtendedSelectionHolder extendedHolder, int i) {
        TextView itemTitle = extendedHolder.itemTitle;
        TextView firstSubsection = extendedHolder.firstSubsection;
        TextView secondSubsection = extendedHolder.secondSubsection;

        RecyclerCompatible toBind = (RecyclerCompatible)database.get(i);

        itemTitle.setText(toBind.getItemTitle());
        firstSubsection.setText(toBind.getFirstSubsection());
        secondSubsection.setText(toBind.getSecondSubsection());
    }


    @Override
    @NonNull
    public ExtendedSelectionHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.extended_selection_item, parent, false);
        return new ExtendedSelectionHolder(view, clickListener);
    }

    @Override
    public int getItemCount() {
        return database.size();
    }

    public void addDatabase(List database) {
        this.database = database;
        notifyDataSetChanged();
    }

    public RecyclerCompatible getItem(int position) {
        return position < database.size() ? (RecyclerCompatible)database.get(position) : null;
    }
}
