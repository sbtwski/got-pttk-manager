package sbtwski.gotpttkmanager.presenter.versatilityItems;

import android.content.Intent;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;

import sbtwski.gotpttkmanager.R;
import sbtwski.gotpttkmanager.model.SpinnerCompatible;
import sbtwski.gotpttkmanager.model.data.Constants;
import sbtwski.gotpttkmanager.presenter.ReconnectActivity;

/**
 * Aktywność bazowa zapewniająca obsługę pobierania danych z serwera do list rozwijanych.
 */

public class SpinnerFetchingActivity extends BasicDrawerActivity {

    public void fetchData(final Intent forActivity, String dataLocation, final Class<? extends SpinnerCompatible> itemsClass) {
        String url = Constants.SERVER_IP + dataLocation;

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        ArrayList<SpinnerCompatible> dataFromJson = new ArrayList<>();
                        ObjectMapper jsonMapper = new ObjectMapper();

                        for(int i=0; i<response.length();i++) {
                            try {
                                SpinnerCompatible current = jsonMapper.readValue(response.getString(i), itemsClass);
                                dataFromJson.add(current);
                            }
                            catch (JSONException e){
                                Log.e("JSON Response","getString failed");
                            }
                            catch (IOException e) {
                                Log.e("JSON Response", "readValue failed");
                                e.printStackTrace();
                            }
                        }

                        RequestDataManager rdm = RequestDataManager.getInstance();
                        rdm.setActualSpinnerDataSaved(dataFromJson);
                        RequestDataManager.requestCounter = 0;
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Intent toReconnect = new Intent(getApplicationContext(), ReconnectActivity.class);
                        toReconnect.putExtra("message", getText(R.string.database_reconnect));
                        toReconnect.putExtra("logged", true);
                        startActivity(toReconnect);
                    }
                });

        ConnectionSingleton.getInstance(this).addToRequestQueue(jsonArrayRequest);
        RequestDataManager.requestCounter++;

        startActivity(forActivity);
        overridePendingTransition(R.anim.slide_in_up,R.anim.slide_out_up);
    }

    public void fetchDataLocally(String dataLocation, final Class<? extends SpinnerCompatible> itemsClass) {
        RequestDataManager.requestCounter = 1;
        String url = Constants.SERVER_IP + dataLocation;

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        ArrayList<SpinnerCompatible> dataFromJson = new ArrayList<>();
                        ObjectMapper jsonMapper = new ObjectMapper();

                        for(int i=0; i<response.length();i++) {
                            try {
                                SpinnerCompatible current = jsonMapper.readValue(response.getString(i), itemsClass);
                                dataFromJson.add(current);
                            }
                            catch (JSONException e){
                                Log.e("JSON Response","getString failed");
                            }
                            catch (IOException e) {
                                Log.e("JSON Response", "readValue failed");
                                e.printStackTrace();
                            }
                        }

                        RequestDataManager rdm = RequestDataManager.getInstance();
                        rdm.setActualSpinnerDataSaved(dataFromJson);
                        RequestDataManager.requestCounter = 0;
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Intent toReconnect = new Intent(getApplicationContext(), ReconnectActivity.class);
                        toReconnect.putExtra("message", getText(R.string.database_reconnect));
                        toReconnect.putExtra("logged", true);
                        startActivity(toReconnect);
                    }
                });

        ConnectionSingleton.getInstance(this).addToRequestQueue(jsonArrayRequest);
    }

    public void fetchDataForResult(final Intent forActivity, String dataLocation, final Class<? extends SpinnerCompatible> itemsClass, int resultCode) {
        String url = Constants.SERVER_IP + dataLocation;

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        ArrayList<SpinnerCompatible> dataFromJson = new ArrayList<>();
                        ObjectMapper jsonMapper = new ObjectMapper();

                        for(int i=0; i<response.length();i++) {
                            try {
                                SpinnerCompatible current = jsonMapper.readValue(response.getString(i), itemsClass);
                                dataFromJson.add(current);
                            }
                            catch (JSONException e){
                                Log.e("JSON Response","getString failed");
                            }
                            catch (IOException e) {
                                Log.e("JSON Response", "readValue failed");
                                e.printStackTrace();
                            }
                        }

                        RequestDataManager rdm = RequestDataManager.getInstance();
                        rdm.setActualSpinnerDataSaved(dataFromJson);
                        RequestDataManager.requestCounter = 0;
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Intent toReconnect = new Intent(getApplicationContext(), ReconnectActivity.class);
                        toReconnect.putExtra("message", getText(R.string.database_reconnect));
                        toReconnect.putExtra("logged", true);
                        startActivity(toReconnect);
                    }
                });

        ConnectionSingleton.getInstance(this).addToRequestQueue(jsonArrayRequest);
        RequestDataManager.requestCounter++;

        startActivityForResult(forActivity, resultCode);
        overridePendingTransition(R.anim.slide_in_up,R.anim.slide_out_up);
    }

    public void setupSpinner(ArrayList<SpinnerCompatible> data, int updateCode) { }
}
