package sbtwski.gotpttkmanager.presenter;

import android.view.View;

/**
 * Interfejs, którego implementacja zapewnia obsługę wyboru elementu z listy.
 */

public interface RecyclerViewClickListener {
    void onClick(View view, int position);
}
