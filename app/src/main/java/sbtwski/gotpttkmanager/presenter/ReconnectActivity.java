package sbtwski.gotpttkmanager.presenter;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;
import android.widget.Toast;

import sbtwski.gotpttkmanager.R;
import sbtwski.gotpttkmanager.model.data.Constants;

/**
 * Aktywność informująca użytkownika o próbie nawiązania połączenia z bazą danych lub dostawcą danych lokalizacyjnych.
 */

public class ReconnectActivity extends AppCompatActivity {
    private TextView progressText;
    private long startTime;
    private boolean connected = false, gpsReconnect;
    private Handler progressHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reconnect);

        setupPrompt();
    }

    /**
     * Metoda uniemożliwiająca powrót do poprzedniej aktywności.
     */
    @Override
    public void onBackPressed() {
        Toast.makeText(getApplicationContext(),getResources().getString(R.string.back_lock),Toast.LENGTH_SHORT).show();
    }

    /**
     * Metoda przygotowująca treść powiadomienia oraz obsługująca pasek postępu.
     */
    private void setupPrompt() {
        TextView reconnectMessage = findViewById(R.id.reconnect_message);
        reconnectMessage.setText(getIntent().getStringExtra("message"));

        TextView reconnectHint = findViewById(R.id.reconnect_hint);
        gpsReconnect = getIntent().getBooleanExtra("GPS",false);
        if(gpsReconnect)
            reconnectHint.setText(getText(R.string.gps_hint));

        progressText = findViewById(R.id.progress_time);
        progressHandler = new Handler();

        final Runnable progressRunnable = new Runnable() {
            @Override
            public void run() {
                if(System.currentTimeMillis() - startTime <= Constants.ATTEMPT_TIME && !connected) {
                    int progressTime = (Constants.ATTEMPT_TIME - (int)(System.currentTimeMillis() - startTime))/1000;
                    String progressMessage = progressTime + "";
                    progressText.setText(progressMessage);
                    progressHandler.postDelayed(this, 500);
                }
                else{
                    progressHandler.removeCallbacks(this);
                    Intent toPrompt = new Intent(getApplicationContext(), PromptActivity.class);

                    String message;

                    if(connected)
                        message = getResources().getString(R.string.error_form_success);
                    else{
                        if(gpsReconnect)
                            message = getResources().getString(R.string.gps_failure);
                        else
                            message = getResources().getString(R.string.database_failure);
                    }

                    toPrompt.putExtra("success", connected);
                    toPrompt.putExtra("message", message);
                    toPrompt.putExtra("logged", getIntent().getBooleanExtra("logged", false));

                    startActivity(toPrompt);
                    overridePendingTransition(R.anim.slide_in_up,R.anim.slide_out_up);
                }
            }
        };

        startTime = System.currentTimeMillis();
        progressHandler.post(progressRunnable);
    }
}
