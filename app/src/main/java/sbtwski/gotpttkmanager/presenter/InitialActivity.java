package sbtwski.gotpttkmanager.presenter;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import sbtwski.gotpttkmanager.R;
import sbtwski.gotpttkmanager.presenter.selectionPresenters.BrowseSelectionMethodsActivity;

/**
 * Aktywność początkowa umożliwiająca przejście do części aplikacji wymagającej logowania lub do części ogólnodostępnej.
 */

public class InitialActivity extends AppCompatActivity {
    private Button logonButton, browseButton, exitButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_initial);

        requestPermissionForReadExternalStorage();
        findAll();

        logonButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), LogonActivity.class));
                overridePendingTransition(R.anim.slide_in_up,R.anim.slide_out_up);
            }
        });

        browseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), BrowseSelectionMethodsActivity.class));
                overridePendingTransition(R.anim.slide_in_up,R.anim.slide_out_up);
            }
        });

        exitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InitialActivity.super.onBackPressed();
                overridePendingTransition(R.anim.slide_in_up,R.anim.slide_out_up);
            }
        });
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_down,R.anim.slide_out_down);
    }

    /**
     * Metoda znajdująca wszystkie wykorzystywane przyciski w widoku.
     */
    private void findAll() {
        logonButton = findViewById(R.id.initial_button_1);
        browseButton = findViewById(R.id.initial_button_2);
        exitButton = findViewById(R.id.initial_button_3);
    }

    /**
     * Metoda wywołująca żądanie przydzielenia uprawnień dostępu do pamięci zewnętrznej.
     */
    private void requestPermissionForReadExternalStorage() {
        try {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.ACCESS_FINE_LOCATION}
                    , 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
