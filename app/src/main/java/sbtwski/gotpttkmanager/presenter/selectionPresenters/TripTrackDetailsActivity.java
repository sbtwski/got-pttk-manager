package sbtwski.gotpttkmanager.presenter.selectionPresenters;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import sbtwski.gotpttkmanager.R;
import sbtwski.gotpttkmanager.model.tracks.TrackFromTrip;
import sbtwski.gotpttkmanager.presenter.versatilityItems.BasicDrawerActivity;

/**
 * Aktywność do obsługi wyświetlania trasy będącej częścią wycieczki.
 */

public class TripTrackDetailsActivity extends BasicDrawerActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ViewGroup parent = findViewById(R.id.content_frame);

        View contentView = inflater.inflate(R.layout.activity_trip_track_details, parent, false);
        navigationDrawer.addView(contentView, 0);

        setupView();
    }

    /**
     * Metoda wypełniająca pola tekstowe odpowiednimi danymi oraz przygotowująca obsługę przycisku powrotu.
     */
    private void setupView() {
        TextView detailsHeader = findViewById(R.id.trip_track_details_header);
        TextView firstPointName = findViewById(R.id.trip_track_point_a_name);
        TextView firstPointGroup = findViewById(R.id.trip_track_point_a_group);
        TextView secondPointName = findViewById(R.id.trip_track_point_b_name);
        TextView secondPointGroup = findViewById(R.id.trip_track_point_b_group);
        TextView trackPoints = findViewById(R.id.trip_track_points_value);
        Button returnButton = findViewById(R.id.trip_track_return_button);

        returnButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        TrackFromTrip data = (TrackFromTrip) getIntent().getSerializableExtra("selectedTrack");
        detailsHeader.setText(data.getItemTitle());
        firstPointName.setText(data.getFirstSubsection());
        firstPointGroup.setText(data.getTrack().getPointA().getMountainGroup().getName());
        secondPointName.setText(data.getSecondSubsection());
        secondPointGroup.setText(data.getTrack().getPointB().getMountainGroup().getName());
        trackPoints.setText(data.getPointsText());
    }
}