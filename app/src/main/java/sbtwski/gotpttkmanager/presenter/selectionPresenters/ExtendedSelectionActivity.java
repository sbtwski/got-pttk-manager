package sbtwski.gotpttkmanager.presenter.selectionPresenters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import sbtwski.gotpttkmanager.R;
import sbtwski.gotpttkmanager.model.data.Constants;
import sbtwski.gotpttkmanager.model.data.MountainGroup;
import sbtwski.gotpttkmanager.model.user.Tourist;
import sbtwski.gotpttkmanager.model.tracks.TrackPTTK;
import sbtwski.gotpttkmanager.presenter.RecyclerViewClickListener;
import sbtwski.gotpttkmanager.presenter.userInputPresenters.TrackEditActivity;
import sbtwski.gotpttkmanager.presenter.versatilityItems.ExtendedFetchRunnable;
import sbtwski.gotpttkmanager.presenter.versatilityItems.ExtendedSelectionAdapter;
import sbtwski.gotpttkmanager.presenter.versatilityItems.SpinnerFetchingActivity;

/**
 * Aktywność do obsługi wyboru z listy elementów wzbogaconych o dodatkowe informacje.
 */

public class ExtendedSelectionActivity extends SpinnerFetchingActivity {
    private ExtendedSelectionAdapter selectionAdapter;
    private boolean forTrackSelection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ViewGroup parent = findViewById(R.id.content_frame);

        View contentView = inflater.inflate(R.layout.activity_extended_selection, parent, false);
        navigationDrawer.addView(contentView, 0);
        forTrackSelection = getIntent().getBooleanExtra("forTrackSelection", false);

        setupRecycler();

        Handler readyHandler = new Handler();
        ExtendedFetchRunnable fetchRunnable = new ExtendedFetchRunnable(selectionAdapter, readyHandler);
        readyHandler.post(fetchRunnable);

        setupAppropriateVersion();
    }

    /**
     * Metoda przygotowująca odpowiednie nagłówki i przycisk w zależności od wartości logicznej określającej rodzaj aktywności.
     */
    private void setupAppropriateVersion() {
        TextView selectionHeader = findViewById(R.id.extended_selection_header);

        if(forTrackSelection)
            selectionHeader.setText(getResources().getString(R.string.track_selection_for_edit_header));
        else
            selectionHeader.setText(getResources().getString(R.string.tourist_selection_header));

        Button addNewTrack = findViewById(R.id.extended_selection_additional_button);

        if(forTrackSelection)
            addNewTrack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent toNewTrack = new Intent(getApplicationContext(), TrackEditActivity.class);
                    fetchData(toNewTrack, Constants.MOUNTAIN_GROUPS_ROUTE, MountainGroup.class);
                }
            });
        else
            addNewTrack.setVisibility(View.GONE);
    }

    /**
     * Metoda przygotowująca odpowiednie listy z obsługą wyboru elementu w zależności od wartości logicznej określającej rodzaj aktywności.
     */
    private void setupRecycler() {
        RecyclerViewClickListener listener = new RecyclerViewClickListener() {
            @Override
            public void onClick(View view, int position) {
                if(forTrackSelection) {
                    TrackPTTK selected = (TrackPTTK)selectionAdapter.getItem(position);
                    Intent toEdit = new Intent(getApplicationContext(), TrackEditActivity.class);
                    toEdit.putExtra("selectedTrack",selected);
                    fetchData(toEdit, Constants.MOUNTAIN_GROUPS_ROUTE, MountainGroup.class);
                }
                else {
                    Tourist selected = (Tourist) selectionAdapter.getItem(position);
                    Intent toDetails = new Intent(getApplicationContext(), DetailsWithSelectionActivity.class);
                    toDetails.putExtra("selectedTourist",selected);
                    toDetails.putExtra("forTripDetails",false);
                    startActivity(toDetails);
                }
                overridePendingTransition(R.anim.slide_in_up,R.anim.slide_out_up);
            }
        };

        RecyclerView selectionRecycler = findViewById(R.id.extended_selection_recycler);
        selectionRecycler.setHasFixedSize(true);

        RecyclerView.LayoutManager selectionLayoutManager = new LinearLayoutManager(this);
        selectionRecycler.setLayoutManager(selectionLayoutManager);

        selectionAdapter = new ExtendedSelectionAdapter(listener);
        selectionRecycler.setAdapter(selectionAdapter);
    }
}
