package sbtwski.gotpttkmanager.presenter.selectionPresenters;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import sbtwski.gotpttkmanager.model.data.Constants;
import sbtwski.gotpttkmanager.R;
import sbtwski.gotpttkmanager.model.data.MountainGroup;
import sbtwski.gotpttkmanager.presenter.userInputPresenters.DistanceCriterionActivity;
import sbtwski.gotpttkmanager.presenter.userInputPresenters.PointsCriterionActivity;
import sbtwski.gotpttkmanager.presenter.versatilityItems.RecyclerFetchingActivity;

/**
 * Aktywność do obsługi wyboru sposobu filtorwania punktów do wyświetlenia.
 */

public class BrowseSelectionMethodsActivity extends RecyclerFetchingActivity {
    private Button groupButton, nearButton, pointsButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_browse_selection_methods);

        findAll();

        groupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent toGroupSelection = new Intent(getApplicationContext(), BasicSelectionActivity.class);
                toGroupSelection.putExtra("forPointSelection", false);
                fetchData(toGroupSelection, Constants.MOUNTAIN_GROUPS_ROUTE, MountainGroup.class);
            }
        });

        nearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), DistanceCriterionActivity.class));
                overridePendingTransition(R.anim.slide_in_up,R.anim.slide_out_up);
            }
        });

        pointsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), PointsCriterionActivity.class));
                overridePendingTransition(R.anim.slide_in_up,R.anim.slide_out_up);
            }
        });
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_down,R.anim.slide_out_down);
    }


    /**
     * Metoda znajdująca wszystkie potrzebne przyciski w widoku.
     */
    private void findAll(){
        groupButton = findViewById(R.id.initial_button_1);
        nearButton = findViewById(R.id.initial_button_2);
        pointsButton = findViewById(R.id.initial_button_3);
    }
}
