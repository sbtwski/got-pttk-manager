package sbtwski.gotpttkmanager.presenter.selectionPresenters;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import sbtwski.gotpttkmanager.R;
import sbtwski.gotpttkmanager.model.data.Constants;
import sbtwski.gotpttkmanager.model.data.MountainGroup;
import sbtwski.gotpttkmanager.model.tracks.TrackPTTK;
import sbtwski.gotpttkmanager.model.tracks.TrackPoint;
import sbtwski.gotpttkmanager.presenter.RecyclerViewClickListener;
import sbtwski.gotpttkmanager.presenter.versatilityItems.BasicFetchRunnable;
import sbtwski.gotpttkmanager.presenter.versatilityItems.BasicSelectionAdapter;
import sbtwski.gotpttkmanager.presenter.versatilityItems.RecyclerFetchingActivity;

/**
 * Aktywność do obsługi wyboru z listy podstawowych elementów.
 */

public class BasicSelectionActivity extends RecyclerFetchingActivity {
    private BasicSelectionAdapter selectionAdapter;
    private boolean forPointSelection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_basic_selection);
        forPointSelection = getIntent().getBooleanExtra("forPointSelection", false);

        setupRecycler();
        setupAppropriateVersion();

        Handler readyHandler = new Handler();
        BasicFetchRunnable fetchRunnable = new BasicFetchRunnable(selectionAdapter, readyHandler);
        readyHandler.post(fetchRunnable);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_down,R.anim.slide_out_down);
    }

    /**
     * Metoda przygotowująca odpowiednie nagłówki w zależności od wartości logicznej określającej rodzaj aktywności.
     */
    private void setupAppropriateVersion() {
        TextView selectionHeader = findViewById(R.id.basic_selection_header);
        TextView recyclerHeader = findViewById(R.id.basic_selection_recycler_header);

        if(forPointSelection)
            selectionHeader.setText(getResources().getString(R.string.point_selection_for_display_header));
        else
            selectionHeader.setText(getResources().getString(R.string.group_selection_for_display_header));

        if(forPointSelection)
            recyclerHeader.setText(getIntent().getStringExtra("recyclerHeader"));
        else
            recyclerHeader.setVisibility(View.GONE);
    }

    /**
     * Metoda przygotowująca odpowiednie listy z obsługą wyboru elementu w zależności od wartości logicznej określającej rodzaj aktywności.
     */
    private void setupRecycler() {
        RecyclerViewClickListener listener = new RecyclerViewClickListener() {
            @Override
            public void onClick(View view, int position) {
                if(forPointSelection) {
                    TrackPoint selected = (TrackPoint) selectionAdapter.getItem(position);
                    Intent toPointDetails = new Intent(getApplicationContext(), TrackPointDetailsActivity.class);
                    toPointDetails.putExtra("selectedPoint",selected);

                    String route = Constants.TRACKS_BY_POINT_ID_ROUTE + selected.getIdText();

                    fetchData(toPointDetails, route, TrackPTTK.class);
                }
                else {
                    MountainGroup selected = (MountainGroup) selectionAdapter.getItem(position);
                    Intent toPointSelection = new Intent(getApplicationContext(), BasicSelectionActivity.class);
                    toPointSelection.putExtra("forPointSelection",true);
                    toPointSelection.putExtra("recyclerHeader", headerResolution(selected));

                    String route = Constants.POINTS_BY_GROUP_ID_ROUTE + selected.getIdText();

                    fetchData(toPointSelection, route, TrackPoint.class);
                }

                overridePendingTransition(R.anim.slide_in_up,R.anim.slide_out_up);
            }
        };

        RecyclerView selectionRecycler = findViewById(R.id.basic_selection_recycler);
        selectionRecycler.setHasFixedSize(true);

        RecyclerView.LayoutManager selectionLayoutManager = new LinearLayoutManager(this);
        selectionRecycler.setLayoutManager(selectionLayoutManager);
        selectionAdapter = new BasicSelectionAdapter(listener);

        selectionRecycler.setAdapter(selectionAdapter);
    }

    /**
     * Metoda przygotowująca treść nagłówków dla aktywności wywoływanej z obecnej.
     */
    private String headerResolution(MountainGroup selectedGroup) {
        String result = getIntent().getStringExtra("recyclerHeader");

        if(result == null)
            result = selectedGroup.getName();
        else
            result = selectedGroup.getName() + System.lineSeparator() + result;

        return result;
    }


}
