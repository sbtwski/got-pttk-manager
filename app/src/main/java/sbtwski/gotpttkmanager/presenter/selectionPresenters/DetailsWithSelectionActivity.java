package sbtwski.gotpttkmanager.presenter.selectionPresenters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

import sbtwski.gotpttkmanager.R;
import sbtwski.gotpttkmanager.model.user.Tourist;
import sbtwski.gotpttkmanager.model.tracks.TrackFromTrip;
import sbtwski.gotpttkmanager.model.user.Trip;
import sbtwski.gotpttkmanager.presenter.RecyclerViewClickListener;
import sbtwski.gotpttkmanager.presenter.versatilityItems.BasicDrawerActivity;
import sbtwski.gotpttkmanager.presenter.versatilityItems.ExtendedSelectionAdapter;

/**
 * Aktywność do obsługi wyboru z listy elementów, z dodatkowymi polami do wyświetlania informacji.
 */

public class DetailsWithSelectionActivity extends BasicDrawerActivity {
    private ExtendedSelectionAdapter selectionAdapter;
    private boolean forTripDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ViewGroup parent = findViewById(R.id.content_frame);

        View contentView = inflater.inflate(R.layout.activity_details_with_selection, parent, false);
        navigationDrawer.addView(contentView, 0);
        forTripDetails = getIntent().getBooleanExtra("forTripDetails", false);

        setupRecycler();
        setupAppropriateVersion();
        setupDetails();
    }

    /**
     * Metoda przygotowująca wyświetlane szczegóły w zależności od wartości logicznej określającej rodzaj aktywności.
     */
    private void setupDetails() {
        TextView detailsHeader = findViewById(R.id.details_header);
        TextView detailsSubsection1Value = findViewById(R.id.details_subsection_1_value);
        TextView detailsSubsection2Value = findViewById(R.id.details_subsection_2_value);
        TextView detailsSubsection3Value = findViewById(R.id.details_subsection_3_value);
        TextView detailsSubsection4Value = findViewById(R.id.details_subsection_4_value);

        DateFormat formatter = new SimpleDateFormat("dd.MM.yyyy", Locale.UK);

        if(forTripDetails){
            Trip source = (Trip)getIntent().getSerializableExtra("selectedTrip");
            detailsHeader.setText(source.getName());
            detailsSubsection1Value.setText(source.getIdText());
            detailsSubsection2Value.setText(formatter.format(source.getStartDate()));
            detailsSubsection3Value.setText(formatter.format(source.getEndDate()));
            detailsSubsection4Value.setText(source.getPointsText());
        }
        else{
            Tourist source = (Tourist)getIntent().getSerializableExtra("selectedTourist");
            detailsHeader.setText(source.getItemTitle());
            detailsSubsection1Value.setText(source.getFirstSubsection());
            detailsSubsection2Value.setText(formatter.format(source.getBirthDate()));
            detailsSubsection3Value.setText(source.getResidenceCity().getName());
            detailsSubsection4Value.setText(source.getResidenceProvince().getName());
        }
    }

    /**
     * Metoda przygotowująca odpowiednie nagłówki i etykiety w zależności od wartości logicznej określającej rodzaj aktywności.
     */
    private void setupAppropriateVersion() {
        TextView recyclerHeader = findViewById(R.id.details_selection_header);
        TextView detailsSubsection1Label = findViewById(R.id.details_subsection_1_label);
        TextView detailsSubsection2Label = findViewById(R.id.details_subsection_2_label);
        TextView detailsSubsection3Label = findViewById(R.id.details_subsection_3_label);
        TextView detailsSubsection4Label = findViewById(R.id.details_subsection_4_label);

        if(forTripDetails) {
            recyclerHeader.setText(getResources().getString(R.string.trip_details_selection_header));
            detailsSubsection1Label.setText(getText(R.string.trip_details_subsection_1_label));
            detailsSubsection2Label.setText(getText(R.string.trip_details_subsection_2_label));
            detailsSubsection3Label.setText(getText(R.string.trip_details_subsection_3_label));
            detailsSubsection4Label.setText(getText(R.string.trip_details_subsection_4_label));
        }
        else {
            recyclerHeader.setText(getResources().getString(R.string.tourist_details_selection_header));
            detailsSubsection1Label.setText(getText(R.string.tourist_details_subsection_1_label));
            detailsSubsection2Label.setText(getText(R.string.tourist_details_subsection_2_label));
            detailsSubsection3Label.setText(getText(R.string.tourist_details_subsection_3_label));
            detailsSubsection4Label.setText(getText(R.string.tourist_details_subsection_4_label));
        }
    }

    /**
     * Metoda przygotowująca odpowiednie listy z obsługą wyboru elementu w zależności od wartości logicznej określającej rodzaj aktywności.
     */
    private void setupRecycler() {
        RecyclerViewClickListener listener = new RecyclerViewClickListener() {
            @Override
            public void onClick(View view, int position) {
                if(forTripDetails) {
                    TrackFromTrip selected = (TrackFromTrip) selectionAdapter.getItem(position);
                    Intent toDetails = new Intent(getApplicationContext(), TripTrackDetailsActivity.class);
                    toDetails.putExtra("selectedTrack",selected);
                    startActivity(toDetails);
                }
                else {
                    Trip selected = (Trip) selectionAdapter.getItem(position);
                    Intent toDetails = new Intent(getApplicationContext(), DetailsWithSelectionActivity.class);
                    toDetails.putExtra("selectedTrip",selected);
                    toDetails.putExtra("forTripDetails", true);
                    startActivity(toDetails);
                }

                overridePendingTransition(R.anim.slide_in_up,R.anim.slide_out_up);
            }
        };

        RecyclerView selectionRecycler = findViewById(R.id.details_selection_recycler);
        selectionRecycler.setHasFixedSize(true);

        RecyclerView.LayoutManager selectionLayoutManager = new LinearLayoutManager(this);
        selectionRecycler.setLayoutManager(selectionLayoutManager);

        selectionAdapter = new ExtendedSelectionAdapter(listener);

        if(forTripDetails) {
            Trip source = (Trip)getIntent().getSerializableExtra("selectedTrip");
            selectionAdapter.addDatabase(source.getTracks());
        }
        else {
            Tourist source = (Tourist) getIntent().getSerializableExtra("selectedTourist");
            selectionAdapter.addDatabase(source.getTrips());
        }

        selectionRecycler.setAdapter(selectionAdapter);
    }
}
