package sbtwski.gotpttkmanager.presenter.selectionPresenters;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import sbtwski.gotpttkmanager.R;
import sbtwski.gotpttkmanager.model.data.MountainGroup;
import sbtwski.gotpttkmanager.model.tracks.TrackPoint;
import sbtwski.gotpttkmanager.presenter.RecyclerViewClickListener;
import sbtwski.gotpttkmanager.presenter.versatilityItems.BasicFetchRunnable;
import sbtwski.gotpttkmanager.presenter.versatilityItems.BasicSelectionAdapter;
import sbtwski.gotpttkmanager.presenter.versatilityItems.RecyclerFetchingActivity;

/**
 * Aktywność do obsługi wyboru grupy górskiej, w której znajdują się trasy spełniające kryterium punktowe określone przez użytkownika.
 */

public class GroupSelectionWithPointsActivity extends RecyclerFetchingActivity {
    private BasicSelectionAdapter selectionAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_basic_selection);

        setupRecycler();
        setupAppropriateVersion();

        Handler readyHandler = new Handler();
        BasicFetchRunnable fetchRunnable = new BasicFetchRunnable(selectionAdapter, readyHandler);
        readyHandler.post(fetchRunnable);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_down,R.anim.slide_out_down);
    }

    /**
     * Metoda przygotowująca nagłówki.
     */
    private void setupAppropriateVersion() {
        TextView selectionHeader = findViewById(R.id.basic_selection_header);
        TextView recyclerHeader = findViewById(R.id.basic_selection_recycler_header);

        selectionHeader.setText(getResources().getString(R.string.group_selection_for_display_header));
        recyclerHeader.setVisibility(View.GONE);
    }

    /**
     * Metoda przygotowująca odpowiednie listy z obsługą wyboru elementu w zależności od wartości logicznej określającej rodzaj aktywności.
     */
    private void setupRecycler() {
        RecyclerViewClickListener listener = new RecyclerViewClickListener() {
            @Override
            public void onClick(View view, int position) {
                MountainGroup selected = (MountainGroup) selectionAdapter.getItem(position);
                Intent toPointSelection = new Intent(getApplicationContext(), BasicSelectionActivity.class);
                toPointSelection.putExtra("forPointSelection",true);
                toPointSelection.putExtra("recyclerHeader", getHeader(selected));

                String routePartFromPrevious = getIntent().getStringExtra("nextRoute");
                routePartFromPrevious += selected.getIdText();
                fetchData(toPointSelection, routePartFromPrevious, TrackPoint.class);
            }
        };

        RecyclerView selectionRecycler = findViewById(R.id.basic_selection_recycler);
        selectionRecycler.setHasFixedSize(true);

        RecyclerView.LayoutManager selectionLayoutManager = new LinearLayoutManager(this);
        selectionRecycler.setLayoutManager(selectionLayoutManager);
        selectionAdapter = new BasicSelectionAdapter(listener);

        selectionRecycler.setAdapter(selectionAdapter);
    }

    /**
     * Metoda przygotowująca treść nagłówków dla aktywności wywoływanej z obecnej.
     */
    private String getHeader(MountainGroup selectedGroup) {
        String result = getIntent().getStringExtra("recyclerHeader");

        if(result == null)
            result = selectedGroup.getName();
        else
            result = selectedGroup.getName() + System.lineSeparator() + result;

        return result;
    }
}

