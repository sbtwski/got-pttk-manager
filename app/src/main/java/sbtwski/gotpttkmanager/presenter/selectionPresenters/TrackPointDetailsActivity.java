package sbtwski.gotpttkmanager.presenter.selectionPresenters;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import sbtwski.gotpttkmanager.R;
import sbtwski.gotpttkmanager.model.tracks.TrackPoint;
import sbtwski.gotpttkmanager.presenter.versatilityItems.TrackPointDetailsAdapter;
import sbtwski.gotpttkmanager.presenter.versatilityItems.TrackPointFetchRunnable;

/**
 * Aktywność do obsługi wyświetlania szczegółów dotyczących punktu trasy.
 */

public class TrackPointDetailsActivity extends AppCompatActivity {
    private TrackPoint selectedPoint;
    private TrackPointDetailsAdapter detailsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_track_point_details);

        selectedPoint = (TrackPoint) getIntent().getSerializableExtra("selectedPoint");

        setupRecycler();
        setupDetails();

        Handler readyHandler = new Handler();
        TextView databaseSize = findViewById(R.id.track_point_details_subsection_2_value);
        TrackPointFetchRunnable fetchRunnable = new TrackPointFetchRunnable(detailsAdapter, databaseSize, readyHandler);
        readyHandler.post(fetchRunnable);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_down,R.anim.slide_out_down);
    }

    /**
     * Metoda wypełniająca nagłówki treścią.
     */
    private void setupDetails() {
        TextView detailsHeader = findViewById(R.id.track_point_details_header);
        TextView firstSubsectionValue = findViewById(R.id.track_point_details_subsection_1_value);

        detailsHeader.setText(selectedPoint.getName());
        firstSubsectionValue.setText(selectedPoint.getAltitudeText());
    }

    /**
     * Metoda przygotowująca listę tras których częścią jest wyświetlany punkt.
     */
    private void setupRecycler() {
        RecyclerView selectionRecycler = findViewById(R.id.track_point_details_recycler);
        selectionRecycler.setHasFixedSize(true);

        RecyclerView.LayoutManager selectionLayoutManager = new LinearLayoutManager(this);
        selectionRecycler.setLayoutManager(selectionLayoutManager);

        detailsAdapter = new TrackPointDetailsAdapter();

        selectionRecycler.setAdapter(detailsAdapter);
    }
}
