package sbtwski.gotpttkmanager.presenter;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

/**
 * Aktywność wyświetlająca logo przy uruchomieniu.
 */

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Thread splash = new Thread() {
            public void run() {
                try {
                    sleep(450);
                } catch (Exception e) {
                    Log.e("SplashThread", "Interrupted");
                } finally {
                    startActivity(new Intent(SplashActivity.this, InitialActivity.class));
                    finish();
                }

            }
        };
        splash.start();
    }
}
