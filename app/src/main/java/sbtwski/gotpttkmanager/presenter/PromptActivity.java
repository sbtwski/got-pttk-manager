package sbtwski.gotpttkmanager.presenter;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import sbtwski.gotpttkmanager.R;
import sbtwski.gotpttkmanager.presenter.selectionPresenters.BrowseSelectionMethodsActivity;

/**
 * Aktywność informująca użytkownika o powodzeniu lub niepowodzeniu wykonanej akcji.
 */

public class PromptActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prompt);

        setupPrompt();
    }

    /**
     * Metoda uniemożliwiająca powrót do poprzedniej aktywności.
     */
    @Override
    public void onBackPressed() {
        Toast.makeText(getApplicationContext(),getResources().getString(R.string.back_lock),Toast.LENGTH_SHORT).show();
    }

    /**
     * Metoda przygotowująca treść powiadomienia oraz obsługę przycisku przejścia dalej.
     */
    private void setupPrompt() {
        TextView promptMessage = findViewById(R.id.prompt_message);
        Button proceedButton = findViewById(R.id.prompt_proceed_button);

        proceedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(getIntent().getBooleanExtra("logged", false)) {
                    Intent toHub = new Intent(getApplicationContext(), CentralHubActivity.class);
                    toHub.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(toHub);
                }
                else {
                    Intent toBrowse = new Intent(getApplicationContext(), BrowseSelectionMethodsActivity.class);
                    toBrowse.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(toBrowse);
                }
                overridePendingTransition(R.anim.slide_in_up,R.anim.slide_out_up);
            }
        });

        boolean previousOperationSuccess = getIntent().getBooleanExtra("success", false);
        String givenMessage = getIntent().getStringExtra("message");

        if(!previousOperationSuccess)
            promptMessage.setTextColor(getResources().getColor(R.color.colorError));

        promptMessage.setText(givenMessage);
    }
}
