package sbtwski.gotpttkmanager.presenter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import sbtwski.gotpttkmanager.R;
import sbtwski.gotpttkmanager.presenter.userInputPresenters.ErrorFormActivity;
import sbtwski.gotpttkmanager.presenter.userInputPresenters.BasicSearchActivity;
import sbtwski.gotpttkmanager.presenter.versatilityItems.BasicDrawerActivity;

/**
 * Aktywność wyboru akcji po zalogowaniu.
 */

public class CentralHubActivity extends BasicDrawerActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ViewGroup parent = findViewById(R.id.content_frame);

        View contentView = inflater.inflate(R.layout.activity_central_hub, parent, false);
        navigationDrawer.addView(contentView, 0);

        TextView greetingHeader = findViewById(R.id.hub_greeting_header);
        String headerText = getText(R.string.hub_greeting) + ", " + getText(R.string.menu_panel_user_data);
        greetingHeader.setText(headerText);

        setupButtons();
    }

    /**
     * Metoda przygotowująca obsługę przycisków przejścia do innych aktywności.
     */
    private void setupButtons() {
        Button checkButton = findViewById(R.id.hub_button_3);
        checkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent forTouristSearch = new Intent(getApplicationContext(), BasicSearchActivity.class);
                forTouristSearch.putExtra("forTrackSearch", false);
                startActivity(forTouristSearch);
                overridePendingTransition(R.anim.slide_in_up,R.anim.slide_out_up);
            }
        });

        Button modifyButton = findViewById(R.id.hub_button_4);
        modifyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent forTrackSearch = new Intent(getApplicationContext(), BasicSearchActivity.class);
                forTrackSearch.putExtra("forTrackSearch", true);
                startActivity(forTrackSearch);
                overridePendingTransition(R.anim.slide_in_up,R.anim.slide_out_up);
            }
        });

        Button errorButton = findViewById(R.id.hub_button_5);
        errorButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), ErrorFormActivity.class));
                overridePendingTransition(R.anim.slide_in_up,R.anim.slide_out_up);
            }
        });
    }
}
