package sbtwski.gotpttkmanager.presenter.userInputPresenters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;

import sbtwski.gotpttkmanager.R;
import sbtwski.gotpttkmanager.model.SpinnerCompatible;
import sbtwski.gotpttkmanager.model.data.Constants;
import sbtwski.gotpttkmanager.model.data.MountainGroup;
import sbtwski.gotpttkmanager.model.tracks.TrackPTTK;
import sbtwski.gotpttkmanager.model.user.Tourist;
import sbtwski.gotpttkmanager.presenter.ReconnectActivity;
import sbtwski.gotpttkmanager.presenter.selectionPresenters.ExtendedSelectionActivity;
import sbtwski.gotpttkmanager.presenter.versatilityItems.ConnectionSingleton;
import sbtwski.gotpttkmanager.presenter.versatilityItems.RecyclerFetchingLoggedActivity;
import sbtwski.gotpttkmanager.presenter.versatilityItems.RequestDataManager;

/**
 * Aktywność do obsługi wyszukiwania tras i turystów.
 */

public class BasicSearchActivity extends RecyclerFetchingLoggedActivity {
    private boolean forTrackSearch;
    private EditText searchInput;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ViewGroup parent = findViewById(R.id.content_frame);

        View contentView = inflater.inflate(R.layout.activity_basic_search, parent, false);
        navigationDrawer.addView(contentView, 0);

        forTrackSearch = getIntent().getBooleanExtra("forTrackSearch", false);
        searchInput = findViewById(R.id.basic_search_input);

        setupDescriptions();
        setupUserInput();
    }

    /**
     * Metoda przygotowująca opisy pól do wprowadzania danych.
     */
    private void setupDescriptions() {
        TextView searchHeader, searchLabel;
        searchHeader = findViewById(R.id.basic_search_header);
        searchLabel = findViewById(R.id.basic_search_input_label);

        if(forTrackSearch){
            searchHeader.setText(getResources().getString(R.string.track_search_header));
            searchLabel.setText(getResources().getString(R.string.track_search_input_label));
        }
        else{
            searchHeader.setText(getResources().getString(R.string.tourist_search_header));
            searchLabel.setText(getResources().getString(R.string.tourist_search_input_label));
        }
    }

    /**
     * Metoda przygotowująca obsługę przycisków potwierdzenia i dodania nowej trasy oraz walidująca wprowadzone dane przed przejściem do kolejnej aktywności.
     */
    private void setupUserInput(){
        Button confirmSearch = findViewById(R.id.basic_search_confirm_button);
        confirmSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(searchInput.getText().toString().isEmpty())
                    Toast.makeText(getApplicationContext(), getText(R.string.generic_empty_input_hint), Toast.LENGTH_SHORT).show();
                else {
                    Intent toSelection = new Intent(getApplicationContext(), ExtendedSelectionActivity.class);
                    String route;
                    if (forTrackSearch) {
                        toSelection.putExtra("forTrackSelection", true);
                        route = decodeTrackRoute(searchInput.getText().toString());
                        fetchData(toSelection, route, TrackPTTK.class);
                    } else {
                        toSelection.putExtra("forTrackSelection", false);
                        route = decodeTouristRoute(searchInput.getText().toString());
                        fetchData(toSelection, route, Tourist.class);
                    }
                }
            }
        });

        Button addNewTrack = findViewById(R.id.basic_search_additional_button);
        if(forTrackSearch)
            addNewTrack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent toNewTrack = new Intent(getApplicationContext(), TrackEditActivity.class);
                    fetchSpinnerData(toNewTrack, Constants.MOUNTAIN_GROUPS_ROUTE, MountainGroup.class);
                }
            });
        else
            addNewTrack.setVisibility(View.GONE);
    }

    /**
     * Metoda przygotowująca odpowiednią trasę przesyłaną w żądaniu do serwera.
     */
    private String decodeTrackRoute(String userInput) {
        String resultRoute;
        if(userInput.matches("^[0-9]+$"))
            resultRoute = Constants.TRACKS_BY_ID_ROUTE + userInput;
        else
            resultRoute = Constants.TRACKS_BY_NAME_ROUTE + userInput;
        return resultRoute;
    }

    /**
     * Metoda przygotowująca odpowiednią trasę przesyłaną w żądaniu do serwera.
     */
    private String decodeTouristRoute(String userInput) {
        String resultRoute;
        if(userInput.matches("^[0-9]+$"))
            resultRoute = Constants.TOURISTS_BY_ID_ROUTE + userInput;
        else
            resultRoute = Constants.TOURISTS_BY_NAME_ROUTE + userInput;
        return  resultRoute;
    }

    /**
     * Metoda służąca do wysyłania żądania do serwera i przetwarzania odpowiedzi.
     */
    private void fetchSpinnerData(final Intent forActivity, String dataLocation, final Class<? extends SpinnerCompatible> itemsClass) {
        String url = Constants.SERVER_IP + dataLocation;

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        ArrayList<SpinnerCompatible> dataFromJson = new ArrayList<>();
                        ObjectMapper jsonMapper = new ObjectMapper();

                        for(int i=0; i<response.length();i++) {
                            try {
                                SpinnerCompatible current = jsonMapper.readValue(response.getString(i), itemsClass);
                                dataFromJson.add(current);
                            }
                            catch (JSONException e){
                                Log.e("JSON Response","getString failed");
                            }
                            catch (IOException e) {
                                Log.e("JSON Response", "readValue failed");
                                e.printStackTrace();
                            }
                        }

                        RequestDataManager rdm = RequestDataManager.getInstance();
                        rdm.setActualSpinnerDataSaved(dataFromJson);
                        RequestDataManager.requestCounter = 0;
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Intent toReconnect = new Intent(getApplicationContext(), ReconnectActivity.class);
                        toReconnect.putExtra("message", getText(R.string.database_reconnect));
                        toReconnect.putExtra("logged", true);
                        startActivity(toReconnect);
                    }
                });

        ConnectionSingleton.getInstance(this).addToRequestQueue(jsonArrayRequest);
        RequestDataManager.requestCounter++;

        startActivity(forActivity);
        overridePendingTransition(R.anim.slide_in_up,R.anim.slide_out_up);
    }
}
