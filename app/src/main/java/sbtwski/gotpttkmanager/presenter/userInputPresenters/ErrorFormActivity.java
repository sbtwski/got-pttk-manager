package sbtwski.gotpttkmanager.presenter.userInputPresenters;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.Random;

import sbtwski.gotpttkmanager.R;
import sbtwski.gotpttkmanager.model.data.Constants;
import sbtwski.gotpttkmanager.model.data.ErrorPriority;
import sbtwski.gotpttkmanager.model.tracks.PointCoordinates;
import sbtwski.gotpttkmanager.model.user.Tourist;
import sbtwski.gotpttkmanager.model.user.TrackError;
import sbtwski.gotpttkmanager.presenter.versatilityItems.BasicDrawerActivity;
import sbtwski.gotpttkmanager.presenter.versatilityItems.FormRunnable;
import sbtwski.gotpttkmanager.presenter.versatilityItems.RequestDataManager;
import sbtwski.gotpttkmanager.presenter.versatilityItems.SendDataThread;

/**
 * Aktywność do obsługi formularza do zgłaszania błędu w trasie.
 */

public class ErrorFormActivity extends BasicDrawerActivity implements CompoundButton.OnCheckedChangeListener, LocationListener {
    //Tourist is for mockup things only, we're not implementing registration and user authorization process - thats why i use mockup
    private Tourist tourist = new Tourist(2, null, 0, 0, false, false, null, null,
            null, null, null,null, null, null);

    private Button photoButton, sendButton;
    private ImageView locationIcon;
    private TextView locationLabel;
    private EditText descriptionInput, locationInput;
    private RadioButton lowestButton, lowButton, mediumButton, highButton, highestButton;
    private boolean gpsEnabled = false;
    private Handler readyHandler;
    public static File evidence = null;
    private double longitude, latitude;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ViewGroup parent = findViewById(R.id.content_frame);

        getSharedPreferences("success", Context.MODE_PRIVATE).edit().putBoolean("succ", false)
                .apply();

        View contentView = inflater.inflate(R.layout.activity_error_form, parent, false);
        navigationDrawer.addView(contentView, 0);

        performSetups();
        locationSetup();
    }

    /**
     * Metoda obsługująca zmianę wybranego przycisku priorytetu.
     */
    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
    {
        lowestButton.setChecked(false);
        lowButton.setChecked(false);
        mediumButton.setChecked(false);
        highButton.setChecked(false);
        highestButton.setChecked(false);

        buttonView.setChecked(isChecked);
    }

    /**
     * Metoda obsługująca rezultat wywołania aktywności wyboru zdjęcia.
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK){
            Uri targetUri = data.getData();

            if(targetUri != null)
                evidence = new File(getRealPathFromURI(targetUri));
        }
    }

    /**
     * Metoda przygotowująca obsługę pobierania danych lokalizacyjnych.
     */
    private void locationSetup(){
        LocationManager gpsManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (ContextCompat.checkSelfPermission( this, Manifest.permission.ACCESS_FINE_LOCATION ) == PackageManager.PERMISSION_GRANTED)
            if (gpsManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                setupLayoutWithGPS();
                gpsEnabled = true;

                gpsManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 2000, 10, this);
            }


    }

    /**
     * Metoda wywoływania w przypadku wykrycia zmian lokalizacji.
     */
    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {
            longitude = location.getLongitude();
            latitude = location.getLatitude();
        }
    }

    /**
     * Metoda wywoływania w przypadku wykrycia zmian statusu modułu GPS.
     */
    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    /**
     * Metoda wywoływania w przypadku wykrycia uruchomienia dostawcy danych lokalizacyjnych.
     */
    @Override
    public void onProviderEnabled(String provider) {

    }

    /**
     * Metoda wywoływania w przypadku wykrycia wyłączenia dostawcy danych lokalizacyjnych.
     */
    @Override
    public void onProviderDisabled(String provider) {

    }

    /**
     * Metoda pozyskująca rzeczywistą ścieżkę do wybranego przez użytkownika zdjęcia.
     */
    private String getRealPathFromURI(Uri uri){
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();

        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        String result = cursor.getString(idx);
        cursor.close();

        return result;
    }

    /**
     * Metoda wywołująca wszystkie metody inicjalizujące.
     */
    private void performSetups(){
        findAll();
        setupRadioGroup();
        setupListeners();
    }

    /**
     * Metoda znajdująca wszystkie wykorzystywane elementy w widoku.
     */
    private void findAll() {
        photoButton = findViewById(R.id.error_form_photo_button);
        sendButton = findViewById(R.id.error_form_send_button);
        descriptionInput = findViewById(R.id.error_form_description_input);
        locationInput  =findViewById(R.id.error_form_location_input);

        lowestButton = findViewById(R.id.error_form_radio_lowest);
        lowButton = findViewById(R.id.error_form_radio_low);
        mediumButton = findViewById(R.id.error_form_radio_medium);
        highButton = findViewById(R.id.error_form_radio_high);
        highestButton = findViewById(R.id.error_form_radio_highest);

        locationIcon = findViewById(R.id.error_form_location_icon);
        locationLabel = findViewById(R.id.error_form_location_label);
    }

    /**
     * Metoda ustawiająca stan początkowy przycisków wyboru priorytetu.
     */
    private void setupRadioGroup(){
        lowestButton.setOnCheckedChangeListener(this);
        lowButton.setOnCheckedChangeListener(this);
        mediumButton.setOnCheckedChangeListener(this);
        highButton.setOnCheckedChangeListener(this);
        highestButton.setOnCheckedChangeListener(this);
    }

    /**
     * Metoda przygotowująca obsługę zdarzeń wyboru zdjęcia oraz przejścia dalej, przeprowadzająca walidację danych.
     */
    private void setupListeners() {
        photoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent, 0);
            }});

        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(radioGroupResolution() == Constants.NOTHING_SELECTED)
                    showMessage(R.string.toast_error_form_no_radio_button_selected);
                else{
                    if(descriptionInput.getText().toString().isEmpty())
                        showMessage(R.string.error_form_input_empty_hint);
                    else{
                        if(!gpsEnabled && locationInput.getText().toString().isEmpty())
                            showMessage(R.string.error_form_input_empty_hint);
                        else{
                            readyHandler = new Handler();
                            sendData();
                            FormRunnable runnable = new FormRunnable(readyHandler ,getApplicationContext());
                            readyHandler.post(runnable);
                        }
                    }
                }
            }
        });
    }

    /**
     * Metoda przygotowująca widok w przypadku wykrycia uruchomionego modułu GPS.
     */
    private void setupLayoutWithGPS() {
        locationIcon.setVisibility(View.VISIBLE);
        locationInput.setVisibility(View.GONE);
        locationLabel.setVisibility(View.GONE);
    }

    /**
     * Metoda zwracająca numer wybranego przez użytkownika przycisku priorytetu.
     */
    private int radioGroupResolution() {
        int result;

        if(lowestButton.isChecked()) result = Constants.LOWEST_SELECTED;
        else
            if(lowButton.isChecked()) result = Constants.LOW_SELECTED;
            else
                if(mediumButton.isChecked()) result = Constants.MEDIUM_SELECTED;
                else
                    if(highButton.isChecked()) result = Constants.HIGH_SELECTED;
                    else result = highestButton.isChecked() ? Constants.HIGHEST_SELECTED : Constants.NOTHING_SELECTED;

        return result;
    }

    /**
     * Metoda rozpoczynająca transmisję wprowadzonych danych na serwer.
     */
    private void sendData(){
        TrackError submittedError = prepareData();
        String url = Constants.SERVER_IP + "err";

        RequestDataManager.requestCounter = 1;

        Thread t = new Thread(new SendDataThread(submittedError, url, getApplicationContext()));
        t.start();
    }

    /**
     * Metoda przygotowująca wprowadzone dane do wysłania.
     */
    private TrackError prepareData() {
        String description = descriptionInput.getText().toString();

        if(!gpsEnabled)
            description += locationInput.getText().toString();

        int priority = radioGroupResolution();

        TrackError toSend;

        if(gpsEnabled){
            PointCoordinates longitudeCoordinates = prepareCoordinates(longitude);
            PointCoordinates latitudeCoordinates = prepareCoordinates(latitude);

            toSend = new TrackError(0, null, null, description, tourist, new ErrorPriority(priority, priority), latitudeCoordinates, longitudeCoordinates);
        }
        else
            toSend = new TrackError(0, null, null, description, tourist, new ErrorPriority(priority, priority));

        return toSend;
    }

    /**
     * Metoda wyświetlająca użytkownikowi powiadomienie.
     */
    private void showMessage(int resourceID) {
        Toast.makeText(getApplicationContext(), getText(resourceID), Toast.LENGTH_SHORT).show();
    }

    /**
     * Metoda przeliczająca wartość współrzędnych odczytanych przez moduł GPS na wartości przyjmowane przez bazę danych.
     */
    private PointCoordinates prepareCoordinates(double gpsValue){
        int seconds = (int)Math.round(gpsValue * 3600);
        int degrees = seconds / 3600;
        seconds = Math.abs(seconds % 3600);
        int minutes = seconds / 60;
        seconds = seconds % 60;

        Random generator = new Random();

        return new PointCoordinates(generator.nextInt(100), degrees, minutes, seconds);
    }
}
