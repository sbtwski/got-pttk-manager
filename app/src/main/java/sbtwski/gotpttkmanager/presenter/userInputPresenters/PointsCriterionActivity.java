package sbtwski.gotpttkmanager.presenter.userInputPresenters;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import sbtwski.gotpttkmanager.R;
import sbtwski.gotpttkmanager.model.data.Constants;
import sbtwski.gotpttkmanager.model.data.MountainGroup;
import sbtwski.gotpttkmanager.presenter.selectionPresenters.GroupSelectionWithPointsActivity;
import sbtwski.gotpttkmanager.presenter.versatilityItems.RecyclerFetchingActivity;

/**
 * Aktywność do obsługi określania kryterium punktowego do wyboru tras.
 */

public class PointsCriterionActivity extends RecyclerFetchingActivity implements CompoundButton.OnCheckedChangeListener{
    private EditText pointsInput;
    private RadioButton lowerButton, equalButton, higherButton;
    private final int NOTHING_SELECTED = 0;
    private final int LOWER_SELECTED = 1;
    private final int EQUAL_SELECTED = 2;
    private final int HIGHER_SELECTED = 3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_points_criterion);

        setupInputs();
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_down,R.anim.slide_out_down);
    }

    /**
     * Metoda obsługująca zmianę wybranego przycisku sposobu porównania.
     */
    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        lowerButton.setChecked(false);
        equalButton.setChecked(false);
        higherButton.setChecked(false);

        buttonView.setChecked(isChecked);
    }

    /**
     * Metoda przygotowująca obsługę przycisku potwierdzenia, przeprowadzająca walidację danych.
     */
    private void setupInputs(){
        Button confirmButton = findViewById(R.id.points_criterion_confirm_button);
        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent toGroupSelection = new Intent(getApplicationContext(), GroupSelectionWithPointsActivity.class);

                if(radioGroupResolution() == NOTHING_SELECTED)
                    showMessage(R.string.toast_points_criterion_no_radio_button_selected);
                else{
                    if(pointsInput.getText().toString().isEmpty())
                        showMessage(R.string.toast_points_criterion_no_points_input);
                    else {
                        toGroupSelection.putExtra("recyclerHeader", getHeader());
                        toGroupSelection.putExtra("nextRoute", getRoute());
                        fetchData(toGroupSelection, Constants.MOUNTAIN_GROUPS_ROUTE, MountainGroup.class);
                    }
                }
            }
        });

        pointsInput = findViewById(R.id.points_criterion_points_input);
        lowerButton = findViewById(R.id.points_criterion_radio_lower);
        equalButton = findViewById(R.id.points_criterion_radio_equal);
        higherButton = findViewById(R.id.points_criterion_radio_higher);

        lowerButton.setOnCheckedChangeListener(this);
        equalButton.setOnCheckedChangeListener(this);
        higherButton.setOnCheckedChangeListener(this);
    }

    /**
     * Metoda przygotowująca nagłówek dla kolejnej wywoływanej aktywności.
     */
    private String getHeader() {
        StringBuilder headerForNext = new StringBuilder();
        headerForNext.append(getText(R.string.points_criterion_point_selection_header_beginning));
        headerForNext.append(" ");

        switch(radioGroupResolution()){
            case LOWER_SELECTED:
                headerForNext.append(getText(R.string.points_criterion_point_selection_header_lower));
                headerForNext.append(" ");
                headerForNext.append(getText(R.string.points_criterion_point_selection_header_than));
                break;
            case EQUAL_SELECTED: headerForNext.append(getText(R.string.points_criterion_point_selection_header_equal)); break;
            case HIGHER_SELECTED:
                headerForNext.append(getText(R.string.points_criterion_point_selection_header_higher));
                headerForNext.append(" ");
                headerForNext.append(getText(R.string.points_criterion_point_selection_header_than));
                break;
        }

        headerForNext.append(" ");
        headerForNext.append(pointsInput.getText());

        return headerForNext.toString();
    }

    /**
     * Metoda przygotowująca odpowiednią trasę przesyłaną w żądaniu do serwera.
     */
    private String getRoute() {
        String condition;

        switch (radioGroupResolution()){
            case LOWER_SELECTED: condition = "lower/"; break;
            case EQUAL_SELECTED: condition = "equal/"; break;
            default: condition = "greater/";
        }

        return Constants.MOUNTAIN_GROUPS_WITH_POINT_CONSTRAINTS_ROUTE + pointsInput.getText().toString() + "/" + condition;
    }

    /**
     * Metoda zwracająca numer wybranego przez użytkownika przycisku sposobu porównania.
     */
    private int radioGroupResolution(){
        int result;

        if(lowerButton.isChecked()) result = LOWER_SELECTED;
        else {
            if(equalButton.isChecked()) result = EQUAL_SELECTED;
            else result = higherButton.isChecked() ? HIGHER_SELECTED : NOTHING_SELECTED;
        }

        return result;
    }

    /**
     * Metoda wyświetlająca użytkownikowi powiadomienie.
     */
    private void showMessage(int resourceID) {
        Toast.makeText(getApplicationContext(), getText(resourceID), Toast.LENGTH_SHORT).show();
    }
}
