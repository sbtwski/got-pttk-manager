package sbtwski.gotpttkmanager.presenter.userInputPresenters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

import sbtwski.gotpttkmanager.R;
import sbtwski.gotpttkmanager.model.SpinnerCompatible;
import sbtwski.gotpttkmanager.model.data.Constants;
import sbtwski.gotpttkmanager.model.data.MountainGroup;
import sbtwski.gotpttkmanager.model.tracks.PointCoordinates;
import sbtwski.gotpttkmanager.model.tracks.TrackPoint;
import sbtwski.gotpttkmanager.presenter.versatilityItems.SendPointThread;
import sbtwski.gotpttkmanager.presenter.versatilityItems.SpinnerArrayAdapter;
import sbtwski.gotpttkmanager.presenter.versatilityItems.SpinnerFetchRunnable;
import sbtwski.gotpttkmanager.presenter.versatilityItems.SpinnerFetchingActivity;

/**
 * Aktywność do obsługi dodawania nowego punktu.
 */

public class TrackPointAddActivity  extends SpinnerFetchingActivity implements AdapterView.OnItemSelectedListener {
    private EditText name, altitude, latitudeDegrees, latitudeMinutes, latitudeSeconds;
    private EditText longitudeDegrees, longitudeMinutes, longitudeSeconds;
    private SpinnerArrayAdapter groupAdapter;
    private Spinner mountainGroup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ViewGroup parent = findViewById(R.id.content_frame);

        View contentView = inflater.inflate(R.layout.activity_track_point_add, parent, false);
        navigationDrawer.addView(contentView, 0);

        setupUserInput();

        Handler readyHandler = new Handler();
        SpinnerFetchRunnable fetchRunnable = new SpinnerFetchRunnable(readyHandler, this, Constants.GROUP_SPINNER_UPDATE);
        readyHandler.post(fetchRunnable);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    /**
     * Metoda przygotowująca działanie listy rozwijanej i wypełniająca ją danymi.
     */
    @Override
    public void setupSpinner(ArrayList<SpinnerCompatible> data, int updateCode) {
        super.setupSpinner(data, updateCode);

        groupAdapter = new SpinnerArrayAdapter(this, data);
        Spinner mountainGroup = findViewById(R.id.track_point_add_group_spinner);
        mountainGroup.setAdapter(groupAdapter);
        mountainGroup.setOnItemSelectedListener(this);
    }

    /**
     * Metoda przygotowująca obsługę przycisku potwierdzenia.
     */
    private void setupUserInput(){
        Button confirmAddition = findViewById(R.id.track_point_add_confirm_button);
        confirmAddition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText[] toCheck = {name, altitude, latitudeDegrees, latitudeMinutes, latitudeSeconds, longitudeDegrees, longitudeMinutes, longitudeSeconds};
                if(everythingFilled(toCheck)) {
                    TrackPoint added = prepareData();
                    String route = Constants.SERVER_IP + "point";
                    Thread pushingThread = new Thread(new SendPointThread(added, route, getApplicationContext()));
                    pushingThread.start();


                    Intent returnIntent = getIntent();
                    returnIntent.putExtra("newPoint",added);
                    setResult(Activity.RESULT_OK,returnIntent);

                    try {
                        pushingThread.join();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    finish();
                }
            }
        });

        name = findViewById(R.id.track_point_add_name_input);
        altitude = findViewById(R.id.track_point_add_altitude_input);
        latitudeDegrees = findViewById(R.id.track_point_add_latitude_degrees_input);
        latitudeMinutes = findViewById(R.id.track_point_add_latitude_minutes_input);
        latitudeSeconds = findViewById(R.id.track_point_add_latitude_seconds_input);

        longitudeDegrees = findViewById(R.id.track_point_add_longitude_degrees_input);
        longitudeMinutes = findViewById(R.id.track_point_add_longitude_minutes_input);
        longitudeSeconds = findViewById(R.id.track_point_add_longitude_seconds_input);

        mountainGroup = findViewById(R.id.track_point_add_group_spinner);
        groupAdapter = new SpinnerArrayAdapter(this, new ArrayList<SpinnerCompatible>());
        mountainGroup.setAdapter(groupAdapter);
        mountainGroup.setOnItemSelectedListener(this);
    }

    /**
     * Metoda walidująca wprowadzone dane.
     */
    private boolean everythingFilled(EditText[] inputsToCheck) {
        boolean everythingOK = true;

        for(EditText current: inputsToCheck){
            current.setBackgroundResource(R.drawable.thin_outline_background);
            if(current.getText().toString().isEmpty()){
                current.setBackgroundResource(R.drawable.error_outline_background);
                everythingOK = false;
            }
        }

        if(!everythingOK)
            Toast.makeText(getApplicationContext(), getText(R.string.track_edit_all_fields_must_be_filled), Toast.LENGTH_SHORT).show();

        return everythingOK;
    }

    /**
     * Metoda przygotowująca dane do wysłania na serwer.
     */
    private TrackPoint prepareData() {
        TrackPoint result = new TrackPoint();

        result.setAltitude(Integer.parseInt(altitude.getText().toString()));
        result.setName(name.getText().toString());
        result.setMountainGroup((MountainGroup)mountainGroup.getSelectedItem());

        result.setLatitude(readCoordinates(latitudeDegrees, latitudeMinutes, latitudeSeconds));
        result.setLongitude(readCoordinates(longitudeDegrees, longitudeMinutes, longitudeSeconds));

        return result;
    }

    /**
     * Metoda odczytująca współrzędne geograficzne z pól tekstowych.
     */
    private PointCoordinates readCoordinates(EditText degrees, EditText minutes, EditText seconds) {
        PointCoordinates result = new PointCoordinates();
        result.setDegrees(Integer.parseInt(degrees.getText().toString()));
        result.setMinutes(Integer.parseInt(minutes.getText().toString()));
        result.setSeconds(Float.parseFloat(seconds.getText().toString()));

        return result;
    }
}
