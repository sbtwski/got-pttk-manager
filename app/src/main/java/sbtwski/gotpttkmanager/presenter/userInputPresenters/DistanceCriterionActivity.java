package sbtwski.gotpttkmanager.presenter.userInputPresenters;

import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import sbtwski.gotpttkmanager.R;
import sbtwski.gotpttkmanager.presenter.ReconnectActivity;

/**
 * Aktywność do obsługi określania kryterium odległości do wyboru tras.
 */

public class DistanceCriterionActivity extends AppCompatActivity {
    private EditText distanceInput;
    private LocationManager manager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_distance_criterion);

        setupInputs();
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_down,R.anim.slide_out_down);
    }


    /**
     * Metoda przygotowująca obsługę przycisku potwierdzenia i pobierania danych lokalizacyjnych oraz walidująca wprowadzone dane przed przejściem do kolejnej aktywności.
     */
    private void setupInputs(){
        Button confirmButton = findViewById(R.id.distance_criterion_confirm_button);
        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(distanceInput.getText().toString().isEmpty())
                    showMessage(R.string.toast_distance_criterion_no_distance_input);
                else {
                    if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                        Intent toReconnect = new Intent(getApplicationContext(), ReconnectActivity.class);
                        toReconnect.putExtra("message", getText(R.string.gps_reconnect));
                        toReconnect.putExtra("GPS", true);
                        toReconnect.putExtra("logged", false);
                        startActivity(toReconnect);
                        overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
                    } else
                        showMessage(R.string.gps_hint);
                }
            }
        });

        manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            findViewById(R.id.distance_criterion_location_icon).setVisibility(View.VISIBLE);
        }

        distanceInput = findViewById(R.id.distance_criterion_distance_input);
    }

    /**
     * Metoda wyświetlająca powiadomienie użytkownikowi.
     */
    private void showMessage(int resourceID) {
        Toast.makeText(getApplicationContext(), getText(resourceID), Toast.LENGTH_SHORT).show();
    }
}
