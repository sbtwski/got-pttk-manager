package sbtwski.gotpttkmanager.presenter.userInputPresenters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import sbtwski.gotpttkmanager.R;
import sbtwski.gotpttkmanager.model.SpinnerCompatible;
import sbtwski.gotpttkmanager.model.data.Constants;
import sbtwski.gotpttkmanager.model.data.MountainGroup;
import sbtwski.gotpttkmanager.model.tracks.TrackPTTK;
import sbtwski.gotpttkmanager.model.tracks.TrackPoint;
import sbtwski.gotpttkmanager.presenter.PromptActivity;
import sbtwski.gotpttkmanager.presenter.versatilityItems.EditRunnable;
import sbtwski.gotpttkmanager.presenter.versatilityItems.SendTrackThread;
import sbtwski.gotpttkmanager.presenter.versatilityItems.SpinnerArrayAdapter;
import sbtwski.gotpttkmanager.presenter.versatilityItems.SpinnerFetchRunnable;
import sbtwski.gotpttkmanager.presenter.versatilityItems.SpinnerFetchingActivity;

/**
 * Aktywność do obsługi edytowania danych trasy PTTK.
 */

public class TrackEditActivity extends SpinnerFetchingActivity implements AdapterView.OnItemSelectedListener {
    private EditText pointsUp, pointsDown, trackName;
    private SpinnerArrayAdapter groupAdapter, pointAdapterA, pointAdapterB;
    private TrackPTTK selectedTrack;
    private Spinner pointA, pointAGroup, pointB, pointBGroup;
    private int firstTime;
    private int pointAddedFlag = 0;
    private TrackPoint addedPoint;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ViewGroup parent = findViewById(R.id.content_frame);

        pointAddedFlag = 0;
        firstTime = Constants.SPINNERS_TO_UPDATE;

        View contentView = inflater.inflate(R.layout.activity_track_edit, parent, false);
        navigationDrawer.addView(contentView, 0);

        selectedTrack = (TrackPTTK)(getIntent().getSerializableExtra("selectedTrack"));
        setupUserInput();

        Handler readyHandler = new Handler();
        SpinnerFetchRunnable fetchRunnable = new SpinnerFetchRunnable(readyHandler, this, Constants.GROUP_SPINNER_UPDATE);
        readyHandler.post(fetchRunnable);

        setupData();
    }

    /**
     * Metoda obsługująca rezultat wywołania aktywności dodania nowego punktu.
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if(requestCode == Constants.POINT_A_ADD_REQUEST){
            if(resultCode == Activity.RESULT_OK){
                pointAddedFlag = 1;
                addedPoint = (TrackPoint)data.getSerializableExtra("newPoint");
                Handler readyHandler = new Handler();
                SpinnerFetchRunnable fetchRunnable = new SpinnerFetchRunnable(readyHandler, this, Constants.GROUP_SPINNER_UPDATE);
                readyHandler.post(fetchRunnable);
            }
        }
        else if(requestCode == Constants.POINT_B_ADD_REQUEST){
            if(resultCode == Activity.RESULT_OK) {
                pointAddedFlag = 2;
                addedPoint = (TrackPoint)data.getSerializableExtra("newPoint");
                Handler readyHandler = new Handler();
                SpinnerFetchRunnable fetchRunnable = new SpinnerFetchRunnable(readyHandler, this, Constants.GROUP_SPINNER_UPDATE);
                readyHandler.post(fetchRunnable);
            }

        }
    }

    /**
     * Metoda obsługująca wybór nowego elementu z listy rozwijanej.
     */
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch(parent.getId()){
            case R.id.track_edit_point_a_group_spinner: reloadPoints((MountainGroup)parent.getItemAtPosition(position), true); break;
            case R.id.track_edit_point_b_group_spinner: reloadPoints((MountainGroup)parent.getItemAtPosition(position), false); break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    /**
     * Metoda przygotowująca obsługę przycisku potwierdzenia edycji oraz przycisków dodawania punktów.
     */
    private void setupUserInput(){
        Button confirmEdit = findViewById(R.id.track_edit_confirm_button);
        confirmEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText[] toCheck = {pointsDown, pointsUp, trackName};
                if(everythingFilled(toCheck)) {
                    Intent toPrompt = new Intent(getApplicationContext(), PromptActivity.class);
                    toPrompt.putExtra("success", true);
                    toPrompt.putExtra("message", getResources().getString(R.string.track_edit_transferred_prompt));
                    toPrompt.putExtra("logged", true);

                    Handler sendHandler = new Handler();
                    sendData();


                    EditRunnable runnable = new EditRunnable(sendHandler ,getApplicationContext());
                    sendHandler.post(runnable);

                    overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
                }
            }
        });

        FloatingActionButton addNewPointA = findViewById(R.id.track_edit_point_a_add_button);
        addNewPointA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent toNewPoint = new Intent(getApplicationContext(), TrackPointAddActivity.class);
                fetchDataForResult(toNewPoint, Constants.MOUNTAIN_GROUPS_ROUTE, MountainGroup.class, Constants.POINT_A_ADD_REQUEST);
            }
        });

        FloatingActionButton addNewPointB = findViewById(R.id.track_edit_point_b_add_button);
        addNewPointB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent toNewPoint = new Intent(getApplicationContext(), TrackPointAddActivity.class);
                fetchDataForResult(toNewPoint, Constants.MOUNTAIN_GROUPS_ROUTE, MountainGroup.class, Constants.POINT_B_ADD_REQUEST);
            }
        });

        pointsUp = findViewById(R.id.track_edit_points_up_input);
        pointsDown = findViewById(R.id.track_edit_points_down_input);
        trackName = findViewById(R.id.track_edit_name_input);

        initializeSpinners();
    }


    /**
     * Metoda przygotowująca działanie list rozwijanych.
     */
    @Override
    public void setupSpinner(ArrayList<SpinnerCompatible> data, int updateCode) {
        super.setupSpinner(data, updateCode);

        switch (updateCode){
            case Constants.GROUP_SPINNER_UPDATE:
                groupAdapter = new SpinnerArrayAdapter(this, data);

                if (pointAddedFlag != 2) {
                    pointAGroup.setAdapter(groupAdapter);
                }
                if(firstTime > 0 && selectedTrack != null){
                    selectAppropriateMountainGroupPointA(pointAGroup, data, selectedTrack);
                    firstTime--;
                }

                if(pointAddedFlag == 1){
                    TrackPTTK withNewPoint = new TrackPTTK();
                    withNewPoint.setPointA(addedPoint);
                    selectAppropriateMountainGroupPointA(pointAGroup, data, withNewPoint);
                }


                if (pointAddedFlag != 1)
                    pointBGroup.setAdapter(groupAdapter);

                if(firstTime > 0 && selectedTrack != null){
                    selectAppropriateMountainGroupPointB(pointBGroup, data, selectedTrack);
                    firstTime--;
                }
                if(pointAddedFlag == 2){
                    TrackPTTK withNewPoint = new TrackPTTK();
                    withNewPoint.setPointB(addedPoint);
                    selectAppropriateMountainGroupPointB(pointBGroup, data, withNewPoint);
                }
                break;
            case Constants.POINT_A_SPINNER_UPDATE:
                pointAdapterA = new SpinnerArrayAdapter(this, data);
                pointA.setAdapter(pointAdapterA);
                if(firstTime > 0 && selectedTrack != null){
                    selectAppropriatePointA(pointA, data, selectedTrack);
                    firstTime--;
                }
                if(pointAddedFlag == 1){
                    TrackPTTK withNewPoint = new TrackPTTK();
                    withNewPoint.setPointA(addedPoint);
                    selectAppropriatePointA(pointA, data, withNewPoint);
                    pointAddedFlag = 0;
                }
                break;
            case Constants.POINT_B_SPINNER_UPDATE:
                pointAdapterB = new SpinnerArrayAdapter(this, data);
                pointB.setAdapter(pointAdapterB);
                if(firstTime > 0 && selectedTrack != null){
                    selectAppropriatePointB(pointB, data, selectedTrack);
                    firstTime--;
                }
                if(pointAddedFlag == 2){
                    TrackPTTK withNewPoint = new TrackPTTK();
                    withNewPoint.setPointB(addedPoint);
                    selectAppropriatePointB(pointB, data, withNewPoint);
                    pointAddedFlag = 0;
                }
                break;
        }
    }

    /**
     * Metoda wprowadzająca dane startowe dla list rozwijanych.
     */
    private void initializeSpinners() {
        groupAdapter = new SpinnerArrayAdapter(this, new ArrayList<SpinnerCompatible>());
        pointAdapterA = new SpinnerArrayAdapter(this, new ArrayList<SpinnerCompatible>());
        pointAdapterB = new SpinnerArrayAdapter(this, new ArrayList<SpinnerCompatible>());

        pointAGroup = findViewById(R.id.track_edit_point_a_group_spinner);
        pointA = findViewById(R.id.track_edit_point_a_spinner);
        pointBGroup = findViewById(R.id.track_edit_point_b_group_spinner);
        pointB = findViewById(R.id.track_edit_point_b_spinner);

        pointAGroup.setOnItemSelectedListener(this);
        pointA.setOnItemSelectedListener(this);
        pointBGroup.setOnItemSelectedListener(this);
        pointB.setOnItemSelectedListener(this);

        pointA.setAdapter(pointAdapterA);
        pointB.setAdapter(pointAdapterB);
        pointAGroup.setAdapter(groupAdapter);
        pointBGroup.setAdapter(groupAdapter);
    }

    /**
     * Metoda przygotowująca dane w polach tekstowych na podstawie wybranej wcześniej trasy.
     */
    private void setupData() {
        if(selectedTrack != null) {
            String points = selectedTrack.getPointsUp()+"";
            pointsUp.setText(points);

            points = selectedTrack.getPointsDown()+"";
            pointsDown.setText(points);

            if(!selectedTrack.getName().isEmpty())
                trackName.setText(selectedTrack.getName());
        }
    }

    /**
     * Metoda walidująca wprowadzone dane.
     */
    private boolean everythingFilled(EditText[] inputsToCheck) {
        boolean everythingOK = true;

        for(EditText current: inputsToCheck){
            current.setBackgroundResource(R.drawable.thin_outline_background);
            if(current.getText().toString().isEmpty()){
                current.setBackgroundResource(R.drawable.error_outline_background);
                everythingOK = false;
            }
        }

        if(!everythingOK)
            Toast.makeText(getApplicationContext(), getText(R.string.track_edit_all_fields_must_be_filled), Toast.LENGTH_SHORT).show();

        return everythingOK;
    }

    /**
     * Metoda przeładowująca listę rozwijaną punktów w przypadku zmiany wybranej grupy górskiej.
     */
    private void reloadPoints(MountainGroup newGroup, boolean forPointA){
        String route = Constants.POINTS_BY_GROUP_ID_ROUTE + newGroup.getIdText();
        fetchDataLocally(route, TrackPoint.class);

        Handler pointsHandler = new Handler();
        SpinnerFetchRunnable fetchRunnable;

        if(forPointA)
            fetchRunnable = new SpinnerFetchRunnable(pointsHandler, this, Constants.POINT_A_SPINNER_UPDATE);
        else
            fetchRunnable = new SpinnerFetchRunnable(pointsHandler, this, Constants.POINT_B_SPINNER_UPDATE);

        pointsHandler.post(fetchRunnable);
    }

    /**
     * Metoda wybierająca odpowiednią grupę górską dla pierwszego punktu na podstawie wybranej wcześniej trasy.
     */
    private void selectAppropriateMountainGroupPointA(Spinner spinner, List<SpinnerCompatible> list, TrackPTTK selectedTrack){
        int size = list.size();
        for(int i = 0; i<size; i++) {
            if (list.get(i).getName().equals(selectedTrack.getPointA().getMountainGroup().getName())) {
                spinner.setSelection(i);
            }
        }
    }

    /**
     * Metoda wybierająca odpowiednią grupę górską dla drugiego punktu na podstawie wybranej wcześniej trasy.
     */
    private void selectAppropriateMountainGroupPointB(Spinner spinner, List<SpinnerCompatible> list, TrackPTTK selectedTrack){
        int size = list.size();
        for(int i = 0; i<size; i++) {
            if (list.get(i).getName().equals(selectedTrack.getPointB().getMountainGroup().getName())) {
                spinner.setSelection(i);
            }
        }
    }

    /**
     * Metoda wybierająca odpowiedni pierwszy punkt na podstawie wybranej wcześniej trasy.
     */
    private void selectAppropriatePointA(Spinner spinner, List<SpinnerCompatible> list, TrackPTTK selectedTrack){
        int size = list.size();
        for(int i = 0; i<size; i++) {
            if (list.get(i).getName().equals(selectedTrack.getPointA().getName())) {
                spinner.setSelection(i);
            }
        }
    }

    /**
     * Metoda wybierająca odpowiedni drugi punkt na podstawie wybranej wcześniej trasy.
     */
    private void selectAppropriatePointB(Spinner spinner, List<SpinnerCompatible> list, TrackPTTK selectedTrack){
        int size = list.size();
        for(int i = 0; i<size; i++) {
            if (list.get(i).getName().equals(selectedTrack.getPointB().getName())) {
                spinner.setSelection(i);
            }
        }
    }

    /**
     * Metoda rozpoczynająca wysyłanie danych na serwer.
     */
    private void sendData(){
        TrackPTTK editedTrack = prepareData();
        String url = Constants.SERVER_IP + "track";

        Thread sendThread = new Thread(new SendTrackThread(editedTrack, url, getApplicationContext(), selectedTrack));
        sendThread.start();
        try {
            sendThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Metoda przygotowująca dane do wysłania na serwer.
     */
    private TrackPTTK prepareData(){
        TrackPTTK track = new TrackPTTK(0, trackName.getText().toString(), null, null,
                null, Integer.parseInt(pointsUp.getText().toString()),
                Integer.parseInt(pointsDown.getText().toString()), null);

        track.setPointA((TrackPoint)pointA.getSelectedItem());
        track.setPointB((TrackPoint)pointB.getSelectedItem());

        return track;
    }

}
