package sbtwski.gotpttkmanager.presenter;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import sbtwski.gotpttkmanager.R;

/**
 * Aktywność obsługująca logowanie w aplikacji.
 */

public class LogonActivity extends AppCompatActivity {
    private EditText loginInput, passwordInput;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logon);

        Button loginButton = findViewById(R.id.logon_login_button);
        prepareInputs();

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(loginInput.getText().toString().isEmpty())
                    showMessage(R.string.logon_login_empty_hint);
                else{
                    if(passwordInput.getText().toString().isEmpty())
                        showMessage(R.string.logon_password_empty_hint);
                    else{
                        startActivity(new Intent(getApplicationContext(), CentralHubActivity.class));
                        overridePendingTransition(R.anim.slide_in_up,R.anim.slide_out_up);
                    }
                }
            }
        });
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_down,R.anim.slide_out_down);
    }

    /**
     * Metoda przygotowująca dane testowe w polach tekstowych.
     */
    private void prepareInputs() {
        loginInput = findViewById(R.id.logon_login_input);
        passwordInput = findViewById(R.id.logon_password_input);

        loginInput.setText(getText(R.string.logon_example_login));
        passwordInput.setText(getText(R.string.logon_example_password));
    }

    /**
     * Metoda wyświetlająca powiadomienie użytkownikowi.
     */
    private void showMessage(int resourceID) {
        Toast.makeText(getApplicationContext(), getText(resourceID), Toast.LENGTH_SHORT).show();
    }
}
