package sbtwski.gotpttkmanager;

import org.junit.Rule;
import org.junit.Test;
import android.content.Intent;

import androidx.test.espresso.intent.rule.IntentsTestRule;
import sbtwski.gotpttkmanager.presenter.userInputPresenters.ErrorFormActivity;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.intent.Intents.intended;
import static androidx.test.espresso.intent.matcher.IntentMatchers.hasAction;

import static org.hamcrest.core.AllOf.allOf;

public class ErrorFormIntentTest {
    @Rule
    public IntentsTestRule<ErrorFormActivity> intentsTestRule =
            new IntentsTestRule<>(ErrorFormActivity.class);
    @Test
    public void selectPhoto(){
        onView(withId(R.id.error_form_photo_button)).perform(click());
        intended(allOf(hasAction(Intent.ACTION_PICK)));
    }
}
