package sbtwski.gotpttkmanager;

import android.app.UiAutomation;
import android.content.Context;
import android.location.LocationManager;
import android.os.Build;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import androidx.test.filters.LargeTest;
import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.rule.ActivityTestRule;
import sbtwski.gotpttkmanager.presenter.CentralHubActivity;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.swipeLeft;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.fail;

@LargeTest
public class CentralHubTest {
    private LocationManager manager;

    @Rule
    public ActivityTestRule<CentralHubActivity> mActivityRule =
            new ActivityTestRule<>(CentralHubActivity.class);

    @Before
    public void prepareManager(){
        manager = (LocationManager)getInstrumentation().getTargetContext().getSystemService(Context.LOCATION_SERVICE);
    }

    @Test
    public void drawerAccessibilityTest(){
        onView(withId(R.id.toolbar_menu_button)).perform(click());
        onView(withId(R.id.navigation_drawer_header_label)).check(matches(isDisplayed()));
        onView(withId(R.id.drawer_layout)).perform(swipeLeft());

        onView(withId(R.id.navigation_drawer_header_label)).check(matches(not(isDisplayed())));
    }

    @Test
    public void touristSelectionTest(){
        onView(withId(R.id.hub_button_3)).perform(click());
        onView(withId(R.id.basic_search_header)).check(matches(withText("Wyszukaj turystę")));
        onView(withId(R.id.basic_search_input_label)).check(matches(withText("Imię i nazwisko/ID turysty:")));
    }

    @Test
    public void trackSelectionTest(){
        onView(withId(R.id.hub_button_4)).perform(click());
        onView(withId(R.id.basic_search_header)).check(matches(withText("Wyszukaj trasę")));
        onView(withId(R.id.basic_search_input_label)).check(matches(withText("Nazwa/Punkt trasy/ID trasy:")));
    }

    @Test
    public void errorSelectionTest(){
        onView(withId(R.id.hub_button_5)).perform(click());
        onView(withId(R.id.error_form_description_label)).check(matches(withText("Opis błędu:")));
        onView(withId(R.id.error_form_priority_label)).check(matches(withText("Priorytet:")));
        onView(withId(R.id.error_form_location_label)).check(matches(withText("Opis położenia:")));
        onView(withId(R.id.error_form_photo_label)).check(matches(withText("Zdjęcie:")));
    }

    @Test
    public void errorSelectionNoLocationTest(){
        if(manager.isLocationEnabled())
           fail("Location is enabled system wide");

        onView(withId(R.id.hub_button_5)).perform(click());
        onView(withId(R.id.error_form_location_icon)).check(matches(not(isDisplayed())));
    }

    @Test
    public void errorSelectionLocationTest(){
        locationMockingEnabler();
        enableLocationProvider();
        onView(withId(R.id.hub_button_5)).perform(click());
        onView(withId(R.id.error_form_location_icon)).check(matches(isDisplayed()));
        disableLocationProvider();
    }

    private void locationMockingEnabler(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            UiAutomation auto = getInstrumentation().getUiAutomation();
            auto.executeShellCommand("appops set " + InstrumentationRegistry.getInstrumentation().getTargetContext().getPackageName() + " android:mock_location allow");
        }
    }

    private void enableLocationProvider(){
        manager.addTestProvider(LocationManager.GPS_PROVIDER,false, false,
                false,false, false, false, false,
                android.location.Criteria.POWER_LOW,android.location.Criteria.ACCURACY_FINE);

        manager.setTestProviderEnabled(LocationManager.GPS_PROVIDER,true);
    }

    private void disableLocationProvider(){
        manager.removeTestProvider(LocationManager.GPS_PROVIDER);
    }
}