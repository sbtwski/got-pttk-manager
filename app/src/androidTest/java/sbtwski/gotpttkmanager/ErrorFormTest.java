package sbtwski.gotpttkmanager;

import android.content.Context;
import android.location.LocationManager;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;
import sbtwski.gotpttkmanager.presenter.userInputPresenters.ErrorFormActivity;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.Espresso.pressBack;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.swipeLeft;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;

import static androidx.test.espresso.matcher.RootMatchers.withDecorView;
import static androidx.test.espresso.matcher.ViewMatchers.isChecked;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.isNotChecked;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.not;

@LargeTest
public class ErrorFormTest {
    private LocationManager manager;

    @Rule
    public ActivityTestRule<ErrorFormActivity> mActivityRule =
            new ActivityTestRule<>(ErrorFormActivity.class);

    @Before
    public void prepareManager(){
        manager = (LocationManager)getInstrumentation().getTargetContext().getSystemService(Context.LOCATION_SERVICE);
    }

    @Test
    public void drawerAccessibilityTest(){
        onView(withId(R.id.toolbar_menu_button)).perform(click());
        onView(withId(R.id.navigation_drawer_header_label)).check(matches(isDisplayed()));
        onView(withId(R.id.drawer_layout)).perform(swipeLeft());

        onView(withId(R.id.navigation_drawer_header_label)).check(matches(not(isDisplayed())));
    }

    @Test
    public void descriptionInputTest(){
        onView(withId(R.id.error_form_description_input)).perform(typeText("Test description nr.1"));
        onView(withId(R.id.error_form_description_input)).check(matches(withText("Test description nr.1")));
        pressBack();

        if(!manager.isLocationEnabled()) {
            onView(withId(R.id.error_form_location_input)).perform(typeText("Test description nr.2"));
            onView(withId(R.id.error_form_location_input)).check(matches(withText("Test description nr.2")));
            pressBack();
        }
    }

    @Test
    public void completionTest(){
        descriptionInputTest();
        onView(withId(R.id.error_form_radio_lowest)).perform(click());

        onView(withId(R.id.error_form_send_button)).perform(click());

        onView(withId(R.id.prompt_message)).check(matches(withText("Dziękujemy za zgłoszenie!")));
        onView(withId(R.id.prompt_proceed_button)).perform(click());

        onView(withId(R.id.hub_greeting_header)).check(matches(isDisplayed()));
    }

    @Test
    public void nothingSelectedTest(){
        onView(withId(R.id.error_form_send_button)).perform(click());

        ErrorFormActivity activity = mActivityRule.getActivity();
        onView(withText(R.string.toast_error_form_no_radio_button_selected)).
                inRoot(withDecorView(not(is(activity.getWindow().getDecorView())))).
                check(matches(isDisplayed()));
    }

    @Test
    public void noDescriptionTest(){
        onView(withId(R.id.error_form_radio_lowest)).perform(click());
        onView(withId(R.id.error_form_send_button)).perform(click());

        ErrorFormActivity activity = mActivityRule.getActivity();
        onView(withText(R.string.error_form_input_empty_hint)).
                inRoot(withDecorView(not(is(activity.getWindow().getDecorView())))).
                check(matches(isDisplayed()));
    }

    @Test
    public void oneDescriptionTest(){
        onView(withId(R.id.error_form_radio_lowest)).perform(click());
        onView(withId(R.id.error_form_description_input)).perform(typeText("Test description nr.1"));
        pressBack();
        onView(withId(R.id.error_form_send_button)).perform(click());

        if(!manager.isLocationEnabled()) {
            ErrorFormActivity activity = mActivityRule.getActivity();
            onView(withText(R.string.error_form_input_empty_hint)).
                    inRoot(withDecorView(not(is(activity.getWindow().getDecorView())))).
                    check(matches(isDisplayed()));
        }
    }

    @Test
    public void radioButtonsTest(){
        onView(withId(R.id.error_form_radio_lowest)).perform(click());

        checkRadioChange(R.id.error_form_radio_lowest, R.id.error_form_radio_low);
        checkRadioChange(R.id.error_form_radio_low, R.id.error_form_radio_medium);
        checkRadioChange(R.id.error_form_radio_medium, R.id.error_form_radio_high);
        checkRadioChange(R.id.error_form_radio_high, R.id.error_form_radio_highest);

        onView(withId(R.id.error_form_radio_highest)).check(matches(isChecked()));
    }

    private void checkRadioChange(int switchFrom, int switchTo){
        onView(withId(switchFrom)).check(matches(isChecked()));
        onView(withId(switchTo)).perform(click());

        onView(withId(switchFrom)).check(matches(isNotChecked()));
    }
}
